<!DOCTYPE html>
<?php
include("../../inc/function/connect.php");
include("../../inc/function/mainFunc.php");
require_once '../../mpdf2/vendor/autoload.php';



$startDate            = $_POST['startDate'];
$endDate              = $_POST['endDate'];
$typeSearch           = @$_POST['typeSearch'];
$service_id           = @$_POST['service_id'];
$point_id             = @$_POST['point_id'];
$agencyCode           = @$_POST['agencyCode'];

$startDateStr = DatetoThaiMonth($startDate);
$endDateStr   = DatetoThaiMonth($endDate);

$textDate = "";
if($startDate == $endDate){
  $textDate = "วันที่ ".$startDateStr;
}else{
  $textDate = "ตั้งแต่วันที่ ".$startDateStr." - ".$endDateStr;
}

$startDate  = $_POST['startDate']." 00:00:01";
$endDate    = $_POST['endDate']." 23:59:59";

$con = "";

if($typeSearch == "1")
{
  $str1 = "แยกตามจุดบริการ (กลุ่มเคาน์เตอร์)";
  if($point_id != "")
  {
      $con = " and point_id = '$point_id'";
  }
  $sql  = "SELECT * from t_point_service WHERE agency_code = '$agencyCode' and is_active = 'Y' $con ";
}else{
  $str1 = "แยกตามประเภทบริการ";
  if($service_id != "")
  {
      $con = " and service_id = '$service_id' ";
  }
  $sql  = "SELECT * from t_service_agency WHERE agency_code = '$agencyCode' and is_active = 'Y' $con order by cat_service_id, service_id";
}

$querys     = DbQuery($sql,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];

// echo $imagePath;

$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
$fontDirs = $defaultConfig['fontDir'];

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];

$mpdf = new \Mpdf\Mpdf([
    'mode' => 'utf-8',
    'format' => 'A4',
    'fontDir' => array_merge($fontDirs, [
         '../../font/CSChatThai',
    ]),
    'fontdata' => $fontData + [
        'kanits' => [
            'R' => 'CSChatThai.ttf'
        ]
    ],
    'default_font' => 'kanits'
]);
ob_start();
// print_r($result);
?>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
  <link rel="stylesheet" href="css/pdf.css">
</head>
<body>
  <htmlpageheader name="firstpageheader">
    <div class="header" >
       <table width = "100%">
        <tr class="text-center">
            <td style="text-align:left;width:50px;"><img style="height:40px" src="../../image/logo.png"/></td>
            <td style="text-align:left;"><p>ระบบสนับสุนนการให้บริการ (Queue)</p></td>
            <td style="text-align:right;width:30%"><p>page {PAGENO} of {nb}</p></td>
         </tr>
       </table>
      </div>
  </htmlpageheader>
  <sethtmlpageheader name="firstpageheader" value="on" show-this-page="1" />
    <br>
    <div align="center" style="font-size:24px;font-weight:600">รายงานสรุปจำนวนผู้มาติดต่อที่มารับบริการ</div>
    <div align="center" style="font-size:24px;font-weight:600"><?= $str1 ?></div>
    <div align="center" style="font-size:22px;font-weight:600"><?=$textDate?></div><br>

  <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; overflow: wrap;" width = "100%">
  <thead>
    <tr class="text-center">
      <th class="thStyle" style="width:60px;">ลำดับที่</th>
      <th class="thStyle" >ช่องบริการ</th>
      <th class="thStyle" style="width:180px;">จำนวนผู้มาติดต่อ<br>ที่มารับบริการ (ราย)</th>
      <th class="thStyle" style="width:180px;">จำนวนผู้มาติดต่อ<br>ที่พลาดการรับบริการ (ราย)</th>
      <th class="thStyle" style="width:180px;">จำนวนผู้ได้รับบริการ (ราย)</th>
    </tr>
  </thead>
  <tbody>
    <?php
         $detailTmp = "";
         $serviceT = 0;
         $cencelT  = 0;
        for($i = 0; $i < $dataCount; $i++)
        {

          if($typeSearch == "1")
          {
              $service_id_list = $rows[$i]['service_id_list'];
              $point_id        = $rows[$i]['point_id'];
              $detail          = $rows[$i]['point_detail'];

              $sqls  = "SELECT s.service_channel,
                       SUM(CASE WHEN t.status_queue = 'E' THEN 1 ELSE 0 END) AS service,
                       SUM(CASE WHEN t.status_queue = 'C' THEN 1 ELSE 0 END) AS cencel
                       from t_service_channel s
                       LEFT JOIN t_trans_queue t ON s.service_channel = t.service_channel
                                 and t.service_id in ($service_id_list) and t.agency_code = '$agencyCode'
                                 and t.status_queue in ('E','C') and t.date_start between '$startDate' and '$endDate'
                       WHERE s.agency_code = '$agencyCode' and s.is_active = 'Y' and s.point_id = '$point_id'
                       group by s.service_channel
                       order by s.service_channel";
          }
          else
          {
            $service_id      = $rows[$i]['service_id'];
            $detail          = $rows[$i]['service_name_a'];

            $sqls  = "SELECT s.service_channel,
                     SUM(CASE WHEN t.status_queue = 'E' THEN 1 ELSE 0 END) AS service,
                     SUM(CASE WHEN t.status_queue = 'C' THEN 1 ELSE 0 END) AS cencel
                     from t_service_channel s
                     LEFT JOIN t_trans_queue t ON s.service_channel = t.service_channel
                               and t.service_id = $service_id and t.agency_code = '$agencyCode'
                               and t.status_queue in ('E','C') and t.date_start between '$startDate' and '$endDate'
                     WHERE s.agency_code = '$agencyCode' and s.is_active = 'Y' and find_in_set('$service_id',s.service_id_list) <> 0
                     group by s.service_channel
                     order by s.service_channel";
            //echo $sqls;
          }

            $flag = "";
            if($detailTmp != $detail)
            {
              $flag = "Y";

              if($detailTmp != "" && $i > 0)
              {
                $totalT = $serviceT - $cencelT;
                echo "
                  <tr class=\"text-center\">
                    <td class=\"content\" colspan=\"2\" style=\"text-align:left;\">รวม</td>
                    <td class=\"content\" style=\"text-align:right;\">$serviceT</td>
                    <td class=\"content\" style=\"text-align:right;\">$cencelT</td>
                    <td class=\"content\" style=\"text-align:right;\">$totalT</td>
                  </tr>
                    </tbody>
                  </table>
                  <div class=\"break\"></div>
                  <table border=\"1\" cellspacing=\"0\" style=\"border-collapse:collapse; border:solid #333 1px; overflow: wrap;\" width = \"100%\">
                  <thead>
                  <tr class=\"text-center\">
                      <th class=\"thStyle\" style=\"width:60px;\">ลำดับที่</th>
                      <th class=\"thStyle\" >ช่องบริการ</th>
                      <th class=\"thStyle\" style=\"width:180px;\">จำนวนผู้มาติดต่อ<br>ที่มารับบริการ (ราย)</th>
                      <th class=\"thStyle\" style=\"width:180px;\">จำนวนผู้มาติดต่อ<br>ที่พลาดการรับบริการ (ราย)</th>
                      <th class=\"thStyle\" style=\"width:180px;\">จำนวนผู้ได้รับบริการ (ราย)</th>
                  </tr>
                  </thead>
                  <tbody>
                ";

                $serviceT = 0;
                $cencelT  = 0;
              }
              $detailTmp = $detail;
            }

            if($flag == 'Y')
            {
              echo "
              <tr class=\"text-center\">
                <td class=\"content\" colspan=\"5\" style=\"text-align:left;\">$detail</td>
              </tr>
              ";
            }

            $numShow = 0;

            $query     = DbQuery($sqls,null);
            $json       = json_decode($query, true);
            $errorInfos  = $json['errorInfo'];
            $dataCounts  = $json['dataCount'];
            $row         = $json['data'];

            for($x = 0; $x < $dataCounts; $x++)
            {
              $numShow++;
              $service_channel = $row[$x]['service_channel'];
              $service = $row[$x]['service'];
              $cencel  = $row[$x]['cencel'];
              $total   = $service - $cencel;

              $serviceT += $service;
              $cencelT  += $cencel;
          ?>
          <tr class="text-center">
            <td class="content" ><?=$numShow;?></td>
            <td class="content" style="text-align:center;"><?= $service_channel ?></td>
            <td class="content" style="text-align:right;"><?= $service ?></td>
            <td class="content" style="text-align:right;"><?= $cencel ?></td>
            <td class="content" style="text-align:right;"><?= $total ?></td>
          </tr>
        <?php
      }}
      ?>
    </tbody>
  </table>
</body>
</html>
<?php
$footer = '<div class="foot">
            <table width = "100%">
              <tr class="text-center">
                  <td style="text-align:left;width:50%"><p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'</td>
               </tr>
             </table>
            </div>';

 $header  = '<div class="header" >
              <table width = "100%">
               <tr class="text-center">
                   <td style="text-align:left;width:50px;"><img style="height:40px" src="../../image/logo.png"/></td>
                   <td style="text-align:left;"><p>ระบบสนับสุนนการให้บริการ (Queue)</p></td>
                   <td style="text-align:right;width:30%"><p>page {PAGENO} of {nb}</p></td>
                </tr>
              </table>
             </div>';


 $html = ob_get_contents();
 ob_end_clean();
 ob_clean();
 $stylesheet = file_get_contents('css/pdf.css');
 $mpdf->SetTitle('รายงานสรุปเวลาที่ใช้ในการให้บริการแก่ผู้รับบริการ');
 // $mpdf->StartProgressBarOutput(2);
 // $mpdf->AddPage('P','','','','',left,right,top,10,10,10);
 // $mpdf->AddPage('P','','','','',10,10,12,10,10,10);
 $mpdf->AddPage('P','','','','',10,10,20,15,5,5);
 $mpdf->SetHTMLHeader($header);
 $mpdf->SetHTMLFooter($footer);
 // $mpdf->showWatermarkText = true;
 $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
 $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

 $mpdf->Output('rep02.pdf', \Mpdf\Output\Destination::INLINE);
 // $mpdf->Output('rep01.pdf', \Mpdf\Output\Destination::FILE);
?>

<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$cat_service_id = @$_POST['cat_service_id'];
$agencyCode     = @$_POST['agencyCode'];

$con = "";

if($cat_service_id != "")
{
  	 $con = " and c.cat_service_id = '$cat_service_id'";
}

$sqla   = "SELECT s.* ,c.cat_service_name
           FROM t_service_agency s, t_cat_service c
           where c.is_active not in ('D') and s.cat_service_id = c.cat_service_id and s.agency_code = '$agencyCode' $con
           ORDER BY c.cat_service_id, s.service_id";
//echo  $sqla;
$querya      = DbQuery($sqla,null);
$jsona       = json_decode($querya, true);
$dataCounta  = $jsona['dataCount'];
$rowA        = $jsona['data'];

 //print_r($rowA);
 if($cat_service_id != "")
 {
   $cat_service_name = "ทุกหมวดงาน".$rowA[0]['cat_service_name'];
 }else{
  $cat_service_name = "ทุกงานบริการ";
 }

?>
<select id="catService" class="form-control select2" style="width: 100%;" onchange="getService()">
<?php
  if($dataCounta > 1)
  {
    echo '<option value="">'.$cat_service_name.'</option>';
  }
  for ($s=0; $s < $dataCounta; $s++) {
    $service_name_a  = $rowA[$s]['service_name_a'];
    $service_id    = $rowA[$s]['service_id'];

    echo '<option value="'.$service_id.'">'.$service_name_a.'</option>';
  }
?>
</select>
<script>
$(function() {
  $('.select2').select2();
});
</script>

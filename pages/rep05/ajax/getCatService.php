<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$division_id = @$_POST['division_id'];

$con = "";

if($division_id != "")
{
  	 $con = " and c.division_id = '$division_id'";
}

$sqla   = "SELECT c.* ,d.division_name
           FROM t_cat_service c, t_division d
           where c.is_active not in ('D') and c.division_id = d.division_id $con
           ORDER BY c.division_id, c.cat_service_id";

$querya      = DbQuery($sqla,null);
$jsona       = json_decode($querya, true);
$dataCounta  = $jsona['dataCount'];
$rowA        = $jsona['data'];

 //print_r($rowA);
 if($division_id != "")
 {
   $division_name = "ทุกหมวด".$rowA[0]['division_name'];
 }else{
  $division_name = "ทุกหมวดงานให้บริการ";
 }

?>
<select id="catService" class="form-control select2" style="width: 100%;" onchange="getService()">
<?php
  if($dataCounta > 1)
  {
    echo '<option value="">'.$division_name.'</option>';
  }
  for ($s=0; $s < $dataCounta; $s++) {
    $cat_service_name  = $rowA[$s]['cat_service_name'];
    $cat_service_id    = $rowA[$s]['cat_service_id'];

    echo '<option value="'.$cat_service_id.'">'.$cat_service_name.'</option>';
  }
?>
</select>
<script>
$(function() {
  $('.select2').select2();
});
</script>

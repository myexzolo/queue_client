$(function() {
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        var s_year =  parseInt(start.format('YYYY')) + 543;
        var e_year =  parseInt(end.format('YYYY')) + 543;
        var startDate   = start.format('DD/MM') + "/" + s_year;
        var endDate     = end.format('DD/MM') + "/" + e_year;

        $('#startDate').val(start.format('YYYY-MM-DD'));
        $('#endDate').val(end.format('YYYY-MM-DD'));
        $('#reportrange span').html(startDate + ' - ' + endDate);
    }
    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'วันนี้': [moment(), moment()],
           'เมื่อวาน': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           '7 วันก่อน': [moment().subtract(6, 'days'), moment()],
           '15 วันก่อน': [moment().subtract(15, 'days'), moment()],
           '30 วันก่อน': [moment().subtract(29, 'days'), moment()],
           'เดือนนี้': [moment().startOf('month'), moment().endOf('month')],
           'เดือนก่อน': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'ปีนี้': [moment().startOf('year'), moment().endOf('year')],
           'ปีก่อน': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        },
        locale: {
            "applyLabel": "เลือก",
            "cancelLabel": "ยกเลิก",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "กำหนดช่วงเวลา",
            "daysOfWeek": [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
            ],
            "monthNames": [
                "มกราคม",
                "กุมภาพันธ์",
                "มีนาคม",
                "เมษายน",
                "พฤษภาคม",
                "มิถุนายน",
                "กรกฎาคม",
                "สิงหาคม",
                "กันยายน",
                "ตุลาคม",
                "พฤศจิกายน",
                "ธันวาคม"
            ],
            "firstDay": 1
        }
    }, cb);
    cb(start, end);
    getServiceChannel();
    showTable();
});


function getTypeSearch()
{
  var typeSearch           = $('#typeSearch').val();
  if(typeSearch == "1")
  {
    $('#pointService').val("").trigger('change');
    $('#div_point').show();
    $('#div_service').hide();
  }
  else
  {
    $('#div_point').hide();
    $('#catService').val("").trigger('change');
    $('#div_service').show();
  }
}

function showTable()
{
  var startDate           = $('#startDate').val();
  var endDate             = $('#endDate').val();
  var typeSearch          = $('#typeSearch').val();
  var point_id            = $('#pointService').val();
  var service_id          = $('#catService').val();
  var agencyCode          = $('#agencyCode').val();
  //console.log(startDate);
  $.post( "ajax/showTable.php",{startDate:startDate,endDate:endDate,typeSearch:typeSearch,point_id:point_id,service_id:service_id,agencyCode:agencyCode})
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function getCatService()
{
  var division_id    = $('#division').val();
  $.post( "ajax/getCatService.php",{division_id:division_id})
  .done(function( data ) {
    $("#getCatService").html( data );
    getService();
  });
}

function getService()
{
  var cat_service_id  = $('#catService').val();
  var agencyCode  = $('#agencyCode').val();
  $.post( "ajax/getService.php",{cat_service_id:cat_service_id,agencyCode:agencyCode})
  .done(function( data ) {
    $("#getService").html( data );
  });
}

function getServiceChannel()
{
  var point_id  = $('#pointService').val();
  var agencyCode      = $('#agencyCode').val();
  $.post( "ajax/getServiceChannel.php",{point_id:point_id,agencyCode:agencyCode})
  .done(function( data ) {
    $("#getServiceChannel").html( data );
  });
}

function printPdf()
{
  var startDate           = $('#startDate').val();
  var endDate             = $('#endDate').val();
  var typeSearch          = $('#typeSearch').val();
  var point_id            = $('#pointService').val();
  var service_id          = $('#catService').val();
  var agencyCode          = $('#agencyCode').val();
  var dataCount           = $('#dataCount').val();

  if(dataCount > 0)
  {
    var pram = "?startDate="+ startDate + "&endDate=" + endDate + "&typeSearch=" + typeSearch + "&point_id=" + point_id + "&service_id=" + service_id+ "&agencyCode=" + agencyCode;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }
}

<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
th{
  vertical-align: middle !important;
}
</style>
<?php
$startDate  = $_POST['startDate'];
$endDate    = $_POST['endDate'];

$startDateStr = DatetoThaiMonth($startDate);
$endDateStr   = DatetoThaiMonth($endDate);

$textDate = "";
if($startDate == $endDate){
  $textDate = "วันที่ ".$startDateStr;
}else{
  $textDate = "ตั้งแต่วันที่ ".$startDateStr." - ".$endDateStr;
}

$startDate  = $_POST['startDate']." 00:00:01";
$endDate    = $_POST['endDate']." 23:59:59";

$sql = "SELECT q.agency_code, a.agency_name,
          SUM(CASE WHEN q.type_queue = 1 THEN 1 ELSE 0 END) AS walkin,
          SUM(CASE WHEN q.type_queue = 2 THEN 1 ELSE 0 END) AS reserve,
          SUM(CASE WHEN q.status_queue = 'C' THEN 1 ELSE 0 END) AS cancel,
          count(trans_queue_id ) AS total
        FROM t_trans_queue q, t_agency a
        WHERE q.date_start between '$startDate' and '$endDate' and  q.agency_code = a.agency_code
        GROUP BY q.agency_code
        ORDER BY count(trans_queue_id) DESC
        limit 0,10";
$querys     = DbQuery($sql,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];

?>
<div class="box-body">
<div align="center">
  <p style="font-size:24px;font-weight:600">รายงานสรุปสาขาที่มีผู้เข้ารับบริการมากที่สุด 10 สาขา</p>
  <p style="font-size:22px;font-weight:600"><?=$textDate?></p>
</div>
<input type="hidden" id="dataCount" value="<?=$dataCount?>">
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px;">ลำดับที่</th>
      <th >สำนักงานประกันสังคม สาขา</th>
      <th style="width:150px;">ผู้มาติดต่อทั้งหมด<br>จำนวน (ราย)</th>
      <th style="width:150px;">กดคิวที่ตู้ Kiosk<br>จำนวน (ราย)</th>
      <th style="width:150px;">จองคิวล่วงหน้า<br>จำนวน (ราย)</th>
      <th style="width:130px;">พลาดการรับบริการ<br>จำนวน (ราย)</th>
    </tr>
  </thead>
  <tbody>
  <?php
      for($i = 0; $i < $dataCount; $i++)
      {

      ?>
        <tr class="text-center">
          <td><?=$i+1;?></td>
          <td style="text-align:left;"><?= $rows[$i]['agency_name'] ?></td>
          <td style="text-align:right;"><?= $rows[$i]['total'] ?></td>
          <td style="text-align:right;"><?= $rows[$i]['walkin'] ?></td>
          <td style="text-align:right;"><?= $rows[$i]['reserve'] ?></td>
          <td style="text-align:right;"><?= $rows[$i]['cancel'] ?></td>
        </tr>
      <?php
      }
    ?>
  </tbody>
</table>
</div>
<div class="box-footer">
  <button type="button" class="btn btn-success btn-flat pull-right btn-rep" onclick="printPdf()" >พิมพ์</button>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false
    })
  })
</script>

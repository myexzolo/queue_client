backHome();

function openModal(){
  $('#myModal').modal('toggle');
}
function goPage(page){
  $(".loading").removeClass('none');
  window.location=page;
}



function goSave(service_id,agency_code,service_code,point_id,kpi_time,service_id){
  $('#myModalShow').modal('toggle');
  // console.log(service_id + " ," + agency_code + " ," + service_code + " ," + point_id);
  $.post("service/saveQueue.php",{service_id:service_id,agency_code:agency_code,service_code:service_code,point_id:point_id})
    .done(function( data ) {
      if(data.status == 'success'){
        $.post("pdf/output.php",{id:data.id,service_code:data.service_code,queue_code:data.queue_code,agency_code:agency_code,point_id:point_id,kpi_time:kpi_time,service_id:service_id,ref_queue:data.ref_queue})
          .done(function( data ) {
            //$.smkAlert({text: data.message,type: data.status});
            //alert(data.pdf_name);

            var path = data.pdf_path;
            //

            // path = encodeURIComponent(path);
            //boundAsync.printQueue(path);
            //$('#myModalShow').modal('toggle');
            //goHome();
            // console.log("http://localhost:2001/ws/printQueue/"+path);
            //
            $.get("http://localhost:2021/ws/printQueue/"+path)
              .done(function( data ) {
                $('#myModalShow').modal('toggle');
                goHome();
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
              alert("ไม่สามารถ เชื่อมต่อ printQueue ได้");
              $('#myModalShow').modal('toggle');
              goHome();
            });
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR.responseText);
            goHome();
            // setTimeout(function(){showDisplay();}, 5000);
        });
      }else{
        $('#myModalShow').modal('toggle');
        goHome();
        //$.smkAlert({text: data.message,type: data.status});
      }

  }).fail(function (jqXHR, textStatus, errorThrown) {
      console.log(">>"+JSON.parse(jqXHR.responseText));
      // setTimeout(function(){showDisplay();}, 5000);
  });
}

function goHome()
{
  var url = $('#urlHome').val();
  console.log(url);
  goPage(url);
}

function backHome()
{
  var url = $('#urlHome').val();
  setTimeout(function(){ goPage(url); }, 30000);
}

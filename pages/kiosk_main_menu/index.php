<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../image/logo.png">
    <title>Home : สำนักงานประกันสังคม</title>

    <?php include "../../inc/css.php"; ?>
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body class="main-bg">
    <?php include "../../inc/nav.php"; ?>
    <img class="image-logo image-logo-main" src="../../image/logo.png">
    <p class="res-queue">สำนักงานประกันสังคม<br> ระบบสนับสนุนการให้บริการ (Queue)</p>
    <div class="box-queue-run">

        <div class="row" style="margin-left:50px;margin-right:50px;">
          <?php

              $menuList     = @$_GET['menu'];
              $code         = @$_GET['code'];
              $agency_code  = @$_GET['bcode'];


              $sqlc = "SELECT m.*
                       FROM t_menu m
                       where m.is_active = 'Y' and m.agency_code = '$agency_code' and m.menu_id IN ($menuList)
                       ORDER BY m.menu_code , m.start_time";

              //echo   $sqlc;

              $queryc = DbQuery($sqlc,null);
              $jsonc = json_decode($queryc,true);
              foreach ($jsonc['data'] as $valuec) {

                $start_time   = $valuec['start_time'];
                $end_time     = $valuec['end_time'];
                $type_service = $valuec['type_service'];
                $menu_id      = $valuec['menu_id'];
                $menu_code    = $valuec['menu_code'];
                $menu_name    = $valuec['menu_name'];
                $point_id     = $valuec['point_id'];

                $ds = '2020-01-01 '.$start_time.":00";
                $de = '2020-01-01 '.$end_time.":00";
                $dn = '2020-01-01 '.date('H:i:s');

                //echo   $ds.", ".$de.", ".$dn."<br>";

                $dteStart = new DateTime($ds);
                $dteEnd   = new DateTime($de);
                $dteNow   = new DateTime($dn);

                $dteStart = $dteStart->getTimestamp();
                $dteEnd   = $dteEnd->getTimestamp();
                $dteNow   = $dteNow->getTimestamp();

                //echo $dteStart.", ".$dteEnd.", ".$dteNow."<br />";
                if($dteNow >= $dteStart && $dteNow <= $dteEnd)
                {
                  //echo $type_service;
                  $link = "";
                  $service_id_list  = $valuec['service_id_list'];
                  if($type_service == '1')
                  {
                      $link = "goSave('$menu_id','$agency_code','$menu_code','$point_id','$menu_name')";
                  }else{
                      $serviceArr = explode(",",$service_id_list);
                      if(count($serviceArr) == 1){

                        $sqlc = "SELECT s.*
                                 FROM t_service_agency s
                                 where s.is_active = 'Y' and s.agency_code = '$agency_code' and s.service_id = $serviceArr[0]";


                       $queryc = DbQuery($sqlc,null);
                       $jsonc = json_decode($queryc,true);
                       $rowc  = $jsonc['data'][0];

                       $service_id      = $rowc['service_id'];
                       $service_code_a  = $rowc['service_code_a'];
                       $service_name_a  = $rowc['service_name_a'];
                       $kpi_time        = $rowc['kpi_time_a'];

                        $link = "goSave2('$service_id','$agency_code','$service_code_a','$point_id','$kpi_time','$service_id','$service_name_a')";
                      }else{
                        $link = "goPage('../kiosk_sub_menu/?sil=$service_id_list&bcode=$agency_code&pid=$point_id&code=$code&m=$menuList')";
                      }

                  }
          ?>
              <div class="col-md-6">
                <div class="btn-border" onclick="<?=$link;?>">
                  <h1><?=$valuec['menu_name']?></h1>
                </div>
              </div>
          <?php
                }
            }
            $urlHome = "../kiosk_service/?code=$code&bcode=$agency_code";
            ?>
        </div>
    </div>
    <div class="row" style="margin-left:50px;margin-right:50px;">
      <div class="pull-right">
        <input type="hidden" id="urlHome" value="<?=$urlHome ?>">
        <div class="btn-border2" onclick="goback('<?=$urlHome?>')">
          <h2><i class="icon ion-android-home"></i>&nbsp;&nbsp;หน้าหลัก</h2>
        </div>
      </div>
    </div>


    <div class="modal fade" id="myModalShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-flex" role="document">
        <div class="modal-content" style="background-color: #005c9e;background-image: linear-gradient(to top, #ffffff3b, #8ba2b4);">
            <div class="modal-body" >
              <img src="../../image/icon/ticket.png" alt="">
              <h1 class="modalH">รอรับบัตรคิว</h1>
            </div>
        </div>
      </div>
    </div>

    <?php include "../../inc/footer.php"; ?>
    <?php include "../../inc/js.php"; ?>
    <script src="js/main.js?v=2"></script>
  </body>
</html>

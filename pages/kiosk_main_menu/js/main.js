backHome();
// openModal();

function openModal(){
  $('#myModalShow').modal('toggle');
}

function goPage(page){
  $(".loading").addClass('none');
  window.location=page;
}


function goSave(menu_id,agency_code,menu_code,point_id,menu_name){
  $('#myModalShow').modal('toggle');
  $.post("service/saveQueue.php",{menu_id:menu_id,agency_code:agency_code,menu_code:menu_code,point_id:point_id})
    .done(function( data ) {
      if(data.status == 'success'){
        $.post("pdf/output.php",{id:data.id,menu_code:data.menu_code,queue_code:data.queue_code,agency_code:agency_code,point_id:point_id,menu_name:menu_name,ref_queue:data.ref_queue})
          .done(function( data ) {
            //$.smkAlert({text: data.message,type: data.status});
            var path = data.pdf_path;
            //console.log(path);
            $.get("http://localhost:2021/ws/printQueue/"+path)
              .done(function( data ) {
                console.log(data);
                $('#myModalShow').modal('toggle');
                goHome();
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                alert("ไม่สามารถ เชื่อมต่อ printQueue ได้");
                $('#myModalShow').modal('toggle');
                goHome();
            });
        });
      }else{
        //backHome();
        //$.smkAlert({text: data.message,type: data.status});
      }

  });
}

function goSave2(service_id,agency_code,service_code,point_id,kpi_time,service_id,service_name){
  $('#myModalShow').modal('toggle');
  $.post("service/saveQueue2.php",{service_id:service_id,agency_code:agency_code,service_code:service_code,point_id:point_id})
    .done(function( data ) {
      if(data.status == 'success'){
        $.post("pdf/output2.php",{id:data.id,service_code:data.service_code,queue_code:data.queue_code,agency_code:agency_code,point_id:point_id,kpi_time:kpi_time,service_id:service_id,service_name:service_name,ref_queue:data.ref_queue})
          .done(function( data ) {
            //$.smkAlert({text: data.message,type: data.status});
            //alert(data.pdf_name);
            var path = data.pdf_path;
            //console.log(path);
            // boundAsync.printQueue(path);
            $.get("http://localhost:2021/ws/printQueue/"+path)
              .done(function( data ) {
                console.log(data);
                $('#myModalShow').modal('toggle');
                goHome();
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                alert("ไม่สามารถ เชื่อมต่อ printQueue ได้");
                $('#myModalShow').modal('toggle');
                goHome();
            });
        });
      }else{
        $('#myModalShow').modal('toggle');
        goHome();
      }
  });
}



function goHome()
{
  var url = $('#urlHome').val();
  console.log(url);
  goPage(url);
}

function backHome()
{
  var url = $('#urlHome').val();
  setTimeout(function(){ goPage(url); }, 30000);
}

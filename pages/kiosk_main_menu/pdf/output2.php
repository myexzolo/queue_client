
    <?php
    // session_start();
    // include('../../../inc/function/session.php');
    include('../../../inc/function/connect.php');
    include('../../../inc/function/mainFunc.php');
    header("Content-type:text/html; charset=UTF-8");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    require_once '../../../mpdf2/vendor/autoload.php';



    $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
    $fontDirs = $defaultConfig['fontDir'];

    $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
    $fontData = $defaultFontConfig['fontdata'];

    $mpdf = new \Mpdf\Mpdf([
        'mode' => 'utf-8',
        'format' => 'A4',
        'fontDir' => array_merge($fontDirs, [
             '../font',
        ]),
        'fontdata' => $fontData + [
            'kanits' => [
                'R' => 'CSChatThai.ttf'
            ]
        ],
        'default_font' => 'kanits'
    ]);

    ob_start();

    ?>

    <?php
        $trans_queue_id = $_POST['id'];
        $service_code   = $_POST['service_code'];
        $service_id     = $_POST['service_id'];
        $queue_code     = $_POST['queue_code'];
        $agency_code    = $_POST['agency_code'];
        $point_id       = $_POST['point_id'];
        $kpi_time       = @$_POST['kpi_time'];
        $service_name   = @$_POST['service_name'];
        $ref_queue      = $_POST['ref_queue'];


        // $trans_queue_id = "1";
        // $service_code   = "2";
        // $service_id     = "2";
        // $queue_code     = "2001";
        // $agency_code    = "1001";
        // $point_id       = "1";
        // $kpi_time       = "20";
        // $service_name   = "TEST";

        $sql = "SELECT * FROM t_point_service WHERE point_id = '$point_id' and agency_code = '$agency_code'";
        $query = DbQuery($sql,null);
        $json = json_decode($query,true);
        $row  = $json['data'][0];

        //echo $sql;

        $is_logo         = $row['is_logo'];
        $is_num_service  = $row['is_num_service'];
        $is_etimate_time = $row['is_etimate_time'];
        $is_welcome      = $row['is_welcome'];
        $is_agency       = $row['is_agency'];
        $is_datetime     = $row['is_datetime'];
        $is_message      = $row['is_message'];
        $is_channel      = $row['is_channel'];
        $is_service_name = $row['is_service_name'];
        $message_queue   = $row['message_queue'];


        $sql = "SELECT * FROM t_agency WHERE agency_code = '$agency_code'";
        $query = DbQuery($sql,null);
        $json = json_decode($query,true);
        $row  = $json['data'][0];

        $agency_name = $row['agency_name'];
        $ip_center   = $row['ip_center'];

        //------------------------------------------------//
        $refCode    = urlencode($agency_code.'#A'.$trans_queue_id);
        $refQueue   = "http://".$ip_center."/pages/questionnaire/?ref=".$refCode;

        $date = date('Y-m-d H:i:s');

        $dateFull  = DateTimeThai(date('Y-m-d H:i:s'));
        //
        // $sql = "SELECT count(trans_queue_id) as num
        //         FROM t_trans_queue
        //         WHERE DATE_FORMAT(date_start, '%Y-%m-%d') = DATE_FORMAT('$date', '%Y-%m-%d')
        //              and queue_code LIKE '$service_code%' and queue_code <> '$queue_code' and agency_code = '$agency_code'";
        //

        $dateStart = date('Y-m-d')." 00:00:00";
        $dateEnd  = date('Y-m-d')." 23:59:59";

        $sql = "SELECT count(trans_queue_id) as num
                FROM t_trans_queue
                WHERE date_start between '$dateStart' and '$dateEnd' and date_start < '$date' and status_queue = 'W'
                and queue_code LIKE '$service_code%' and queue_code <> '$queue_code' and agency_code = '$agency_code'";

       $query  = DbQuery($sql,null);
       $num    = json_decode($query,true)['data'][0]['num'];

    ?>

    <div class="pdf-box">
      <?php if($is_logo == "Y") { ?>
      <img class="pdf-img" src="../../../image/logo.png" alt="">
    <?php } if($is_agency == "Y") { ?>
      <h1 class="pdf-s">สำนักงานประกันสังคม</h1>
      <h1 class="pdf-s"><?= str_replace("สำนักงานประกันสังคม","",$agency_name) ?></h1>
    <?php } if($is_agency == "Y") { ?>
      <h1 class="pdf-w">ยินดีต้อนรับ</h1>
    <?php } ?>
      <h1 class="pdf-h"><?=$queue_code ?></h1>
    <?php if($is_service_name == "Y") { ?>
      <h1 class="pdf-q"><?= $service_name ?></h1>
    <?php }
     if($is_channel == "Y") {

            $sqls = "SELECT *
                    FROM t_service_channel
                    WHERE is_active = 'Y' and agency_code = '$agency_code' and point_id = '$point_id' order by service_channel";
            $querys = DbQuery($sqls,null);
            $jsons  = json_decode($querys,true);
            $dataCount = $jsons['dataCount'];

            $channel = "";
            $channel_tmp = 0;
            if($dataCount > 0)
            {
              foreach ($jsons['data'] as $values) {

                $service_id_list = $values['service_id_list'];
                $service_channel = $values['service_channel'];
                $serviceArr      = explode(",",$service_id_list);


                if(in_array($service_id, $serviceArr))
                {
                  if($channel == "")
                  {
                    $channel       = $service_channel;
                    $channel_tmp   = $service_channel;
                    $channel_start = $service_channel;
                  }
                  else
                  {
                    $numC = intval($channel_tmp) + 1;

                    if($service_channel == $numC)
                    {
                       $channel   = $channel_start." - ".$service_channel;
                    }
                    else
                    {
                      $channel  .=  ", ".$service_channel;
                    }
                  }
                  $channel_tmp = $service_channel;
                }
              }
            }
      ?>
      <h1 class="pdf-q">ช่องบริการ <?= $channel ?></h1>
    <?php } if($is_num_service == "Y") { ?>
      <h1 class="pdf-q">มีคิวก่อนหน้าท่าน <?= $num ?> ราย</h1>
    <?php } if($is_etimate_time == "Y") {
          if($kpi_time > 0)
          {
            $e_time  = ($num * $kpi_time);
          }else{
            $e_time = "-";
          }
      ?>
      <h1 class="pdf-q">เวลาที่คาดว่าจะได้รับ <?= $e_time ?> นาที</h1>
    <?php } if($is_datetime == "Y") { ?>
      <h1 class="pdf-q"><?=$dateFull ?></h1>
    <?php } ?>
    <h1 class="pdf-c">-------------------------------------------</h1>
    <h1 class="pdf-c"><barcode code="<?=$refQueue?>" type="QR" size="0.9" error="M" disableborder = "1"/></h1>
    <h1 class="pdf-cc"><?=$ref_queue?></h1>
    <h1 class="pdf-cc">QR Code ใช้สำหรับแสดงความคิดเห็น</h1>
    <?php if($is_message == "Y") { ?>
      <h1 class="pdf-c">-------------------------------------------</h1>
      <h1 class="pdf-m"><?=$message_queue ?></h1>
    <?php } ?>
    </div>
<?php

  $html = ob_get_contents();
  ob_end_clean();
  $stylesheet = file_get_contents('../css/ticket.css');
  $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
  $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);
  // $random = 'PDF_'.time().rand(0,1000);

  $pdf_file = $queue_code.".pdf";

  // $pdf_path = $_SERVER['DOCUMENT_ROOT'].'/queue_client/ticket/'.$pdf_file;

  $pdf_path = $pdf_file ;

  if(!$mpdf->Output("../../../ticket/$pdf_file", \Mpdf\Output\Destination::FILE)){
    header("Content-Type: application/json");
    exit(json_encode(array("status" => "success","message" => "success",'pdf_path' => $pdf_path)));
  }else{
    header("Content-Type: application/json");
    exit(json_encode(array("status" => "danger","message" => "print PDF Fail")));
  }

?>

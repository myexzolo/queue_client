<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../image/logo.png">
    <title>Home : สำนักงานประกันสังคม</title>

    <?php include "../../inc/css.php"; ?>
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body class="main-bg">
    <?php include "../../inc/nav.php"; ?>
    <div class="box-queue-run">
      <div style="font-size:50vmin;color:#fff;width:100%;text-align:center;margin-top:13vmin">
          <?php
              $agency_code   = @$_GET['acode'];

              $sql   = "SELECT * FROM t_agency where is_active = 'Y' and agency_code  = '$agency_code'";
              $query = DbQuery($sql,null);
              $json  = json_decode($query,true);
              $rows  = $json['data'][0];

              $agency_name  = $rows['agency_name'];

              echo $agency_name;
          ?>
     </div>
  </div>
  </body>
  <?php include "../../inc/js.php"; ?>
  <script src="js/main.js"></script>
</html>

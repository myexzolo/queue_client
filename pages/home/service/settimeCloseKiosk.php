<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$ip    = $_POST['ip'];

// $ip    = "192.168.0.247";
// $time  = "15:21";

$arrData['time'] = date("H:i");

$data_array  = array(
                   "functionName" => "settimeClose",  ///แก้ ชื่อ Service
                   "dataJson" => $arrData,
                 );


$url        = "http://$ip/queue_client/ws/service.php";

//echo json_encode($data_array);

$make_call = callAPI('POST', $url, json_encode($data_array));
$response = json_decode($make_call, true);
$status   = $response;

//echo $status;
if($status == "success")
{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'success')));
}else
{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'fail','message' => 'fail')));
}

?>

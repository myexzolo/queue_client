<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px">ลำดับ</th>
      <th style="width:90px">จุดบริการ</th>
      <th style="width:120px">Kiosk Code</th>
      <th>Kiosk Name</th>
      <th style="width:140px">เวลาเปิด-ปิด เครื่อง</th>
      <th style="width:140px">เวลาเปิด-ปิด บริการ</th>
      <th style="width:90px">สถานะ</th>
      <th style="width:50px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:50px;">แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:50px;">ลบ</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $agencyCode = $_SESSION['AGENCY_CODE'];
      $point_id   = $_POST['point_id'];
      $con        = "";
      if($point_id != "")
      {
          $con = " and p.point_id = '$point_id' ";
      }

      $sqls   = "SELECT k.*, p.point_number
                 FROM t_kiosk k,  t_point_service p
                 where k.is_active not in ('D') and k.agency_code = '$agencyCode' and p.agency_code = '$agencyCode'
                 and k.point_id = p.point_id $con
                 ORDER BY point_id , kiosk_code";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0)
      {
        foreach ($rows as $key => $value) {
          $is_active        = $value['is_active'];
          // $mem_username     = $value['mem_username']?;

          $activeTxt  = "";
          $bg         = "";
          $onclick    = "";
          $disabled   = "";

          $activeTxtR  = "";
          $bgR         = "";



          if($is_active == "Y")
          {
            $activeTxt = "ใช้งาน";
            $bg        = "bg-green-active";
          }else if($is_active == "N"){
            $activeTxt = "ไม่ใช้งาน";
            $bg        = "bg-gray";
          }

          // if($reserve_status == "Y")
          // {
          //   $activeTxtR = "เปิดจอง";
          //   $bgR        = "bg-green-active";
          // }else if($reserve_status == "N"){
          //   $activeTxtR = "ไม่เปิดจอง";
          //   $bgR        = "bg-gray";
          // }

    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="center"><?=$value['point_number']?></td>
      <td align="center"><?=$value['kiosk_code']?></td>
      <td ><?=$value['kiosk_name']?></td>
      <td align="center"><?=$value['kiosk_open']." - ".$value['kiosk_close']?></td>
      <td align="center"><?=$value['service_open']." - ".$value['service_close']?></td>
      <td><div class="<?=$bg?>"><?=$activeTxt ?></div></td>
      <td>
        <a class="btn_point text-green"><i class="fa fa-eye" onclick="showForm('VIEW','<?=$value['kiosk_id']?>')"></i></a>
      </td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['kiosk_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['kiosk_id']?>','<?=$value['kiosk_code']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>

$(function () {
  showDisplay();
});

function showDisplay()
{
  var limit = $('#limit').val();
  var serviceIdList = $('#serviceIdList').val();
  var agency_code = $('#agency_code').val();
  var numberCode = $('#numberCode').val();
  var point_id = $('#point_id').val();
  var ip = $('#ip').val();

  $.post("ajax/showDisplay.php",{limit:limit,serviceIdList:serviceIdList,agency_code:agency_code,point_id:point_id,numberCode:numberCode})
    .done(function( data ) {
         $('#show-display').html(data);
         setTimeout(function(){showDisplay();}, 1000);
  }).fail(function (jqXHR, textStatus, errorThrown) {
      //setTimeout(function(){showDisplay();}, 1000);
  });
}

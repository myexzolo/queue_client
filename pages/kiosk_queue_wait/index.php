<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../image/logo.png">
    <title>Home : สำนักงานประกันสังคม</title>

    <?php include "../../inc/css.php"; ?>
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body class="main-bg">
    <?php include "../../inc/nav.php"; ?>
    <div class="box-queue-run">
          <?php

              $pn           = @$_GET['pn'];
              $agency_code  = @$_GET['bcode'];
              $kiosk_code   = @$_GET['kcode'];
              $limit        = @$_GET['m'];


              $sql   = "SELECT * FROM t_kiosk where is_active = 'Y' and kiosk_code = '$kiosk_code'";
              $query = DbQuery($sql,null);
              $json  = json_decode($query,true);
              $rows  = $json['data'][0];

              $ip    = @$rows['ip'];


              $sqlc = "SELECT p.*
                       FROM t_point_service p
                       where p.is_active = 'Y' and p.agency_code = '$agency_code' and p.point_number	= '$pn'";

              //echo   $sqlc;

              $queryc = DbQuery($sqlc,null);
              $jsonc  = json_decode($queryc,true);
              $rows   = $jsonc['data'][0];

              $service_id_list  = $rows['service_id_list'];
              $point_id         = $rows['point_id'];
          ?>
        <input type="hidden" value="<?= $service_id_list ?>" id="serviceIdList">
        <input type="hidden" value="<?= $limit ?>" id="limit">
        <input type="hidden" value="<?= $agency_code ?>" id="agency_code">
        <input type="hidden" value="<?= $point_id ?>" id="point_id">
        <input type="hidden" value="<?= $ip ?>" id="ip">
        <input type="hidden" value="" id="numberCode">
        <div id="show-display"></div>
     </div>
  </body>
  <?php include "../../inc/js.php"; ?>
  <script src="js/main.js"></script>
</html>

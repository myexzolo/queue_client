<?php
include '../../../inc/function/connect.php';
include '../../../inc/function/mainFunc.php';
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$limit          = $_POST['limit'];
$serviceIdList  = $_POST['serviceIdList'];
$agency_code    = $_POST['agency_code'];
$point_id       = $_POST['point_id'];
$numberCode     = $_POST['numberCode'];

// $limit          = '6';
// $serviceIdList  = '1,2,3,4,5,6';
// $agency_code    = '1001';
// $point_id       = '1';
// $numberCode     = '';

$dateStart      = date('Y-m-d')." 00:00:00";
$dateEnd        = date('Y-m-d')." 23:59:59";


$queueCode  = "";
$statusQueue = "";
$serviceChannel = "";
$html  = "";


 $sql   = " SELECT *
            FROM t_trans_queue
            WHERE status_queue = 'W' AND agency_code = '$agency_code'
                  AND date_start between '$dateStart' and '$dateEnd' and point_id = '$point_id'
                  AND
            (case
              when  type_service = 1 then menu_id in (select DISTINCT mn.menu_id
                                                      from t_service_agency sv,t_menu mn
                                                      where find_in_set(sv.service_id,mn.service_id_list) <> 0
                                                          and sv.service_id in ($serviceIdList))
              when  type_service = 2 then service_id in ($serviceIdList)
              else 1
            end)
            ORDER BY date_start asc LIMIT 0,$limit";

  //echo $sql;

  $query  = DbQuery($sql,null);
  $json   = json_decode($query, true);
  $countS     = $json['dataCount'];
  $errorInfo  = $json['errorInfo'];
  $rowQueueS  = $json['data'];

   //print_r($rowQueueS);
   //ob_start();
?>

<div style="width:100%;" class="showDisplay">
  <div class="vertical-box">
    <div class="row">
        <div class="col-md-12 showText">
          <div style="text-align:center ; height: 70px;border-bottom: 1px solid #fff">
          <h1 style="color:#fff; font-size : 60px;">คิวที่รอรับบริการ</h1>
          </div>
        </div>
    </div>
    <div class="row">
        <?php
        for($x=0; $x < $countS ; $x++)
        {
          $trans_queue_id   = $rowQueueS[$x]['trans_queue_id'];
          $ref_code         = $rowQueueS[$x]['ref_code'];
          $queue_code       = $rowQueueS[$x]['queue_code'];
        ?>
          <div class="col-mdx-2 serviceNumber">
            <span ><?= $queue_code?></span>
          </div>
        <?php
        }
        ?>
    </div>
  </div>
</div>

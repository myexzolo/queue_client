backHome();
// openModal();

function openModal(code,agency_code,point_id)
{
  $.post("ajax/number_pad.php",{code:code,agency_code:agency_code,point_id:point_id})
    .done(function( data ) {
      $('#show-form').html(data);
      // $('#myModalRef').modal('toggle');
      $('#myModalRef').modal({backdrop:'static'});
  });
}


function openModalQR(code,agency_code,point_id)
{
  $('#agency_code').val(agency_code);
  $('#point_id').val(point_id);
  $('#code').val(code);
  $('#myModalQR').modal({backdrop:'static'});
  $('#ref_code').val("");
  $('#ref_code').focus();
}

$('#myModalQR').on('shown.bs.modal', function () {
    $('#ref_code').val("");
    $('#ref_code').focus();
})

function setFocus()
{
  $('#ref_code').val("");
  $('#ref_code').focus();
}

function submitFrm()
{

}


$('#ref_code').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) {
    e.preventDefault();
    return false;
  }
  if($('#ref_code').val().length >= 13)
  {
    var agency_code = $('#agency_code').val();
    var ref_code    =  $('#ref_code').val();
    var code        =  $('#code').val();
    var point_id    =  $('#point_id').val();

    var refarr     = ref_code.split("#");
    var agencyCode = refarr[0];
    ref_code       = refarr[1];

    console.log(agencyCode+ " ,"+agency_code+ " ,"+ ref_code);
    if(agency_code == agencyCode)
    {
      $.post("ajax/reserve.php",{code:code,agency_code:agency_code,point_id:point_id,ref_code:ref_code})
        .done(function( res ) {
          console.log(res);
          if(res.status == "success")
          {
            var data = res.data;
            $('#myModalQR').modal('toggle');
            goSave(data.service_id,data.agency_code,data.service_code,data.point_id,data.kpi_time,data.service_name,data.reserve_ref_code,data.member_id);
          }else{
            $('#myModalQR').modal('toggle');
          }
      }).fail(function (jqXHR, textStatus) {
          console.log(textStatus);
      });
    }else{
      $('#refQRCode').html("ไม่พบ รหัสการจอง : "+ ref_code);
      $('#myModalQrnon').modal();
      $('#myModalQR').modal('toggle');
    }
  }
});


function goPage(page){
  $(".loading").addClass('none');
  window.location=page;
}


function goSave(service_id,agency_code,service_code,point_id,kpi_time,service_name,reserve_ref_code,member_id){
  $('#myModalShow').modal('toggle');
  $.post("service/saveQueue.php",{service_id:service_id,agency_code:agency_code,service_code:service_code,point_id:point_id,reserve_ref_code:reserve_ref_code,member_id:member_id})
    .done(function( data ) {
      if(data.status == 'success'){
        $.post("pdf/output.php",{id:data.id,service_code:data.service_code,queue_code:data.queue_code,agency_code:agency_code,point_id:point_id,kpi_time:kpi_time,service_id:service_id,service_name:service_name,ref_queue:data.ref_queue})
          .done(function( data ) {
            //$.smkAlert({text: data.message,type: data.status});
            //alert(data.pdf_name);
            var path = data.pdf_path;
            //console.log(path);
            // boundAsync.printQueue(path);
            $.get("http://localhost:2021/ws/printQueue/"+path)
              .done(function( data ) {
                console.log(data);
                $('#myModalRef').modal('toggle');
                goHome();
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                alert("ไม่สามารถ เชื่อมต่อ printQueue ได้");
                $('#myModalRef').modal('toggle');
                goHome();
            });
        });
      }else{
        $('#myModalRef').modal('toggle');
        goHome();
      }
  });
}



$('#formReserve').on('submit', function(event) {
  event.preventDefault();
  if ($('#formReserve').smkValidate()) {
    $.ajax({
        url: 'ajax/reserve.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( res ) {
        console.log(res);
        if(res.status == "success")
        {
          var data = res.data;
          $('#myModalRef').modal('toggle');
          goSave(data.service_id,data.agency_code,data.service_code,data.point_id,data.kpi_time,data.service_name,data.reserve_ref_code,data.member_id);
        }else{
          alert("ไม่พบรหัสการจองนี้");
          $('#myModalRef').modal('toggle');
        }
    });
  }
});

function goHome()
{
  var url = $('#urlHome').val();
  console.log(url);
  goPage(url);
}

function backHome()
{
  var url = $('#urlHome').val();
  setTimeout(function(){ goPage(url); }, 30000);
}


function setnumber(number)
{
  var val = $("#ref_code").val();
  if(val.length < 8)
  {
    $("#ref_code").val(val+number);
  }
}

function delnum()
{
  var val = $("#ref_code").val();
  if(val.length > 0)
  {
    $("#ref_code").val(val.substring(0, val.length -1));
  }
}

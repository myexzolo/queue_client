<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$code        = $_POST['code'];
$agency_code = $_POST['agency_code'];
$point_id    = $_POST['point_id'];

?>

<div class="modal-body2">
  <div class="row">
    <div class="col-md-12">
        <div class="form-group">
          <div style="margin-right:5px;margin-left:5px;">
            <input type="hidden"  name="agency_code" value="<?= $agency_code ?>">
            <input type="hidden"  name="code" value="<?= $code ?>">
            <input type="hidden"  name="point_id" value="<?= $point_id ?>">
            <input type="text"  class="form-control" id="ref_code" name="ref_code" value="" minlength="8" maxlength="8" required readonly
             style="font-size:60px;width:100%;height:75px;text-align:center;color:#000;" data-smk-msg="&nbsp;">
          </div>
        </div>
    </div>
    <div class="col-md-12">
       <table style="width:100%" class="table text-center">
         <tr>
           <td style="width:33.33%"><button type="button" class="btn btn-block btn-primary btn-flat number-pad" onclick="setnumber('1')">1</button></td>
           <td style="width:33.33%"><button type="button" class="btn btn-block btn-primary btn-flat number-pad" onclick="setnumber('2')">2</button></td>
           <td ><button type="button" class="btn btn-block btn-primary btn-flat number-pad" onclick="setnumber('3')">3</button></td>
         </tr>
         <tr>
           <td><button type="button" class="btn btn-block btn-primary btn-flat number-pad" onclick="setnumber('4')">4</button></td>
           <td><button type="button" class="btn btn-block btn-primary btn-flat number-pad" onclick="setnumber('5')">5</button></td>
           <td><button type="button" class="btn btn-block btn-primary btn-flat number-pad" onclick="setnumber('6')">6</button></td>
         </tr>
         <tr>
           <td><button type="button" class="btn btn-block btn-primary btn-flat number-pad" onclick="setnumber('7')">7</button></td>
           <td><button type="button" class="btn btn-block btn-primary btn-flat number-pad" onclick="setnumber('8')">8</button></td>
           <td><button type="button" class="btn btn-block btn-primary btn-flat number-pad" onclick="setnumber('9')">9</button></td>
         </tr>
         <tr>
           <td><button type="button" class="btn btn-block btn-primary btn-flat number-pad"></button></td>
           <td><button type="button" class="btn btn-block btn-primary btn-flat number-pad" onclick="setnumber('0')">0</button></td>
           <td><button type="button" class="btn btn-block btn-primary btn-flat number-del" onclick="delnum()"><i class="ion ion-backspace"></i></button></td>
         </tr>
       </table>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:130px;font-size:25px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:130px;font-size:25px;">บันทึก</button>
</div>

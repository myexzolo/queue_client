<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../image/logo.png">
    <title>Home : สำนักงานประกันสังคม</title>

    <?php include "../../inc/css.php"; ?>
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body class="main-bg">
    <?php include "../../inc/nav.php"; ?>
    <img class="image-logo image-logo-main" src="../../image/logo.png">
    <p class="res-queue">สำนักงานประกันสังคม<br> ระบบสนับสนุนการให้บริการ (Queue)</p>
    <?php
        $code         = @$_GET['code'];
        $agency_code  = @$_GET['bcode'];
        $point_id     = @$_GET['p'];

        $urlHome = "../kiosk_service/?code=$code&bcode=$agency_code";
    ?>

    <div class="box-home">
      <div class="box" onclick="openModal('<?=$code?>','<?=$agency_code?>','<?= $point_id?>')" style="background-color: #005c9e;">
        <img src="../../image/icon/keypad.png">
        <p>รหัสการจอง</p>
      </div>
      <div class="box" onclick="openModalQR('<?=$code?>','<?=$agency_code?>','<?= $point_id?>')" style="background-color: #005c9e;">
        <img src="../../image/icon/qr-codec.png">
        <p>QR code</p>
      </div>
    </div>
    <div class="row" style="margin-left:50px;margin-right:50px;">
      <div class="pull-right">
        <input type="hidden" id="urlHome" value="<?=$urlHome ?>">
        <div class="btn-border2" onclick="goback('<?=$urlHome?>')">
          <h2><i class="icon ion-android-home"></i>&nbsp;&nbsp;หน้าหลัก</h2>
        </div>
      </div>
    </div>

    <div class="modal fade" id="myModalRef" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-flex" role="document">
        <div class="modal-content" style="background-color: #005c9e;background-image: linear-gradient(to top, #ffffff3b, #8ba2b4);">
          <div class="modal-header">
            <h1 class="modal-title" id="myModalLabel" style="color:#fff">รหัสการจอง</h1>
          </div>
          <form id="formReserve" novalidate enctype="multipart/form-data">
          <!-- <form novalidate enctype="multipart/form-data" action="ajax/reserve.php" method="post"> -->
            <div id="show-form"></div>
          </form>
        </div>
      </div>
    </div>

    <div class="modal fade" id="myModalShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-flex" role="document">
        <div class="modal-content" style="background-color: #005c9e;background-image: linear-gradient(to top, #ffffff3b, #8ba2b4);">
            <div class="modal-body" >
              <img src="../../image/icon/ticket.png" alt="">
              <h1 class="modalH">รอรับบัตรคิว</h1>
            </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="myModalQR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" onclick="setFocus()">
      <div class="modal-dialog modal-flex" role="document">
        <div class="modal-content">
          <form id="formQueue" novalidate>
            <div class="modal-body" id="show-data">
              <img src="../../image/icon/qr-code.png" alt="">
              <h1>Scan Qrcode ด้านล่าง</h1>
                <form id="formReserveQR" novalidate enctype="multipart/form-data">
                  <input type="hidden"  id="agency_code" value="<?= $agency_code ?>">
                  <input type="hidden"  id="code" value="<?= $code ?>">
                  <input type="hidden"  id="point_id" value="<?= $point_id ?>">
                  <input id="ref_code" type="text" class="btn-ref form-control" required>
                </form>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary" id="btnSubmit" style="display:none;">Save Change</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="modal fade" id="myModalQrnon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-flex" role="document">
        <div class="modal-content">
            <div class="modal-body">
              <img src="../../image/icon/queue2.png" alt="">
              <h1><div id="refQRCode">ไม่พบ Qrcode</div></h1>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>

    <?php include "../../inc/footer.php"; ?>
    <?php include "../../inc/js.php"; ?>
    <script src="js/main.js"></script>
  </body>
</html>

<?php
include '../../../inc/function/connect.php';
include '../../../inc/function/mainFunc.php';
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$limit          = $_POST['limit'];
$serviceIdList  = $_POST['serviceIdList'];
$agency_code    = $_POST['agency_code'];
$point_id       = $_POST['point_id'];
$numberCode     = $_POST['numberCode'];

// $limit          = '6';
// $serviceIdList  = '1,2,3,4,5,6';
// $agency_code    = '1001';
// $point_id       = '1';
// $numberCode     = '';

$dateStart      = date('Y-m-d')." 00:00:00";
$dateEnd        = date('Y-m-d')." 23:59:59";


$queueCode  = "";
$statusQueue = "";
$serviceChannel = "";
$html  = "";

$sql   = " SELECT *
           FROM t_trans_queue
           WHERE status_queue = 'R' AND agency_code = '$agency_code'
                 AND date_service between '$dateStart' and '$dateEnd' and point_id = '$point_id'
                 AND
           (case
             when  type_service = 1 then menu_id in (select DISTINCT mn.menu_id
                                                     from t_service_agency sv,t_menu mn
                                                     where find_in_set(sv.service_id,mn.service_id_list) <> 0
                                                         and sv.service_id in ($serviceIdList))
             when  type_service = 2 then service_id in ($serviceIdList)
             else 1
           end)
           ORDER BY date_service LIMIT 0,$limit";

           //echo $sql;

 $query  = DbQuery($sql,null);
 $json   = json_decode($query, true);
 $count      = $json['dataCount'];
 $errorInfo  = $json['errorInfo'];
 $rowQueueR  = $json['data'];

 if($count == 0){
   $rowQueueR = array();
 }

 $limit  -= $count;

 $sql   = " SELECT *
            FROM t_trans_queue
            WHERE status_queue not in ('R') AND agency_code = '$agency_code'
                  AND date_service between '$dateStart' and '$dateEnd' and point_id = '$point_id'
                  AND
            (case
              when  type_service = 1 then menu_id in (select DISTINCT mn.menu_id
                                                      from t_service_agency sv,t_menu mn
                                                      where find_in_set(sv.service_id,mn.service_id_list) <> 0
                                                          and sv.service_id in ($serviceIdList))
              when  type_service = 2 then service_id in ($serviceIdList)
              else 1
            end)
            ORDER BY date_service DESC LIMIT 0,$limit";

            //echo $sql;

  $query  = DbQuery($sql,null);
  $json   = json_decode($query, true);
  $countS     = $json['dataCount'];
  $errorInfo  = $json['errorInfo'];
  $rowQueueS  = $json['data'];
  if($countS > 0)
  {
    for($x = 0; $x < count($rowQueueS); $x++)
    {

       $num = ($count + $x);

       $rowQueueR[$num]['trans_queue_id']   = $rowQueueS[$x]['trans_queue_id'];
       $rowQueueR[$num]['ref_code']         = $rowQueueS[$x]['ref_code'];
       $rowQueueR[$num]['queue_code']       = $rowQueueS[$x]['queue_code'];
       $rowQueueR[$num]['status_queue']     = $rowQueueS[$x]['status_queue'];
       $rowQueueR[$num]['service_channel']  = $rowQueueS[$x]['service_channel'];

    }
  }
  // print_r($rowQueueR);

ob_start();
?>

<div style="width:100%;" class="showDisplay">
  <div class="vertical-box">
    <div class="row">
        <div class="col-md-12 showText">
          <div style="text-align:center ; height: 70px;border-bottom: 1px solid #fff">
          <h1 style="color:#fff; font-size : 60px;">ช่องบริการ</h1>
          </div>
        </div>
        <?php
        for($x=0; $x < count($rowQueueR) ; $x++)
        {
          $trans_queue_id   = $rowQueueR[$x]['trans_queue_id'];
          $ref_code         = $rowQueueR[$x]['ref_code'];
          $queue_code       = $rowQueueR[$x]['queue_code'];
          $status_queue     = $rowQueueR[$x]['status_queue'];
          $service_channel  = $rowQueueR[$x]['service_channel'];

          if($x==0)
          {
             $queueCode   = $queue_code;
             $statusQueue = $status_queue;
             $serviceChannel = $service_channel;
             echo "<input type=\"hidden\" value=\"$queue_code\" id=\"numberCodeTmp\">";
             echo "<input type=\"hidden\" value=\"$service_channel\" id=\"serviceChannel\">";
             echo "<input type=\"hidden\" value=\"$trans_queue_id\" id=\"transQueueId\">";
             echo "<input type=\"hidden\" value=\"$ref_code\" id=\"refCode\">";
             echo "<div class=\"col-box\">
                    <div class=\"serviceBox\" align=\"center\">
                     <div class=\"serviceNumberF\">$service_channel</div>
                    </div>
                   </div>";
          }else{
            echo "<input type=\"hidden\" value=\"\" id=\"numberCodeTmp\">";
            echo "<input type=\"hidden\" value=\"\" id=\"serviceChannel\">";
            echo "<div class=\"col-box\">
                    <div class=\"serviceBox\" align=\"center\">
                      <div class=\"serviceNumber\">$service_channel</div>
                    </div>
                  </div>";
          }
        }
        ?>
    </div>
  </div>
</div>
<?php
$html = ob_get_contents();
ob_end_clean();
if(intval($errorInfo[0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success','html' => $html, 'queue_code' => $queueCode, 'status_queue' => $statusQueue, 'serviceChannel' => $serviceChannel)));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail','html' => "" , 'queue_code' => "")));
}
?>

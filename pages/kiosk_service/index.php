<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../image/logo.png">
    <title>Home : สำนักงานประกันสังคม</title>

    <?php
    include "../../inc/css.php";
    require_once '../../phpqrcode/qrlib.php';
    ?>
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body class="main-bg">
    <?php include "../../inc/nav.php"; ?>

<?php



      $code         = @$_GET['code'];
      $agency_code  = @$_GET['bcode'];

      $text = $code."_".$agency_code;
      $file = "qr/".$text.".png";

      if(!file_exists($file))
      {
        $ecc = 'H';
        $pixel_size = 128;
        $frame_size = 1;

        QRcode::png($text, $file, $ecc, $pixel_size, $frame_size);
      }


      $dateNow = datetoThaiFull(date('Y/m/d'));

      $sql = "SELECT * FROM t_kiosk WHERE kiosk_code = '$code' and agency_code = '$agency_code'";


      $query      = DbQuery($sql,null);
      $json       = json_decode($query, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];

      if($dataCount > 0)
      {
        $data  =   $json['data'][0];

        $service_open =  $data['service_open'];
        $service_close = $data['service_close'];
        $point_id      = $data['point_id'];
        $codeTmp = $code;

        $sql        = "SELECT * FROM t_agency WHERE agency_code = '$agency_code'";
        $query      = DbQuery($sql,null);
        $agency     = json_decode($query,true)['data'][0];

        $_SESSION['IP_CENTER'] = $agency['ip_center'];

        //echo $_SESSION['IP_CENTER'];
        echo "<input type=\"hidden\" id=\"service_open\" value=\"$service_open\">";
        echo "<input type=\"hidden\" id=\"service_close\" value=\"$service_close\">";

        $menu_id_list = $data['menu_id_list'];
        if($menu_id_list != "")
        {
          $menu_id_list = "'".str_replace(",","','",$menu_id_list)."'";

          $sqlc = "SELECT * FROM t_menu WHERE ref_code IN ($menu_id_list) and is_active = 'Y'";
          $queryc = DbQuery($sqlc,null);
          $jsonc = json_decode($queryc,true);

          $menuIdlist = "";
          foreach ($jsonc['data'] as $valuec)
          {
             $menu_id     = $valuec['menu_id'];
             $type_menu   = $valuec['type_menu'];
             $start_time  = $valuec['start_time'];
             $end_time    = $valuec['end_time'];

             $time = $start_time."|".$end_time;

             if($type_menu == 2)
             {
               echo "<input type=\"hidden\" class=\"break\" value=\"$time\">";
             }else{
               if($menuIdlist == "")
               {
                 $menuIdlist = $menu_id;
               }else
               {
                 $menuIdlist .= ",".$menu_id;
               }
             }
          }
        }
      }else{
        $menuIdlist = "";
        $codeTmp = "";
      }
?>
    <img class="image-logo image-logo-main" src="../../image/logo.png">
    <p class="res-queue">สำนักงานประกันสังคม <br> ระบบสนับสนุนการให้บริการ (Queue)</p>
    <div onclick="test()">test</div>
    <div class="box-home">
      <input type="hidden" id="kiosk_code" value="<?= $codeTmp ?>">
      <div class="box" onclick="goback('../kiosk_main_menu/?menu=<?=$menuIdlist?>&code=<?=$code?>&bcode=<?=$agency_code?>')" style="background-color: #005c9e;">
        <img src="../../image/icon/queue.png">
        <p>รับบัตรคิว</p>
      </div>
      <div class="box" onclick="goback('../kiosk_reserve/?menu=<?=$menuIdlist?>&code=<?=$code?>&bcode=<?=$agency_code?>&p=<?=$point_id?>')" style="background-color: #005c9e;">
        <img src="../../image/icon/booking.png">
        <p>นัดล่วงหน้า</p>
      </div>
    </div>
    <div style="font-size: 30px; color:#fff;margin:10px;text-align: right;" ondblclick="closeApp()">
       วันที่ <label style="font-weight: normal;"><?= $dateNow ?></label> เวลา <label style="font-weight: normal;" id="timeNow">00:00:00</label> น.
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModalService" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-flex" role="document">
        <div class="modal-content">
            <div class="modal-body" >
              <img src="../../image/icon/stop-watch.png" alt="">
              <h1>ขออภัยขณะนี้ไม่อยู่ในช่วงเวลาให้บริการ</h1>
              <div id="timeBreak" style="font-size:30px;">12:00 - 13:00 น.</div>
            </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="myModalServiceOpen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-flex" role="document">
        <div class="modal-content">
            <div class="modal-body" >
              <img src="../../image/icon/stop-watch.png" alt="">
              <h1>เปิดให้บริการในช่วงเวลา</h1>
              <div id="timeOpen" style="font-size:30px;"><?=$service_open?> - <?=$service_close ?> น.</div>
            </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-flex" role="document">
        <div class="modal-content">
          <form id="formQueue" novalidate>
            <div class="modal-body" id="show-data">
              <img src="../../image/icon/qr-code.png" alt="">
              <h1>Scan Qrcode ด้านล่าง</h1>
              <input id="user" type="text" name="qrcode" autofocus required>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary" style="display:none;">Save Change</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="modal fade" id="myModalQr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-flex" role="document">
        <div class="modal-content">
            <div class="modal-body" id="show-data">
              <img src="../../image/icon/queue2.png" alt="">
              <h1>ไม่พบ Qrcode นี้</h1>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="myModalKiosk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-flex" role="document">
        <div class="modal-content">
            <div class="modal-body" id="show-data">
              <img src="../../image/icon/queue2.png" alt="">
              <h1>ไม่พบ Kiosk Code : <?= $agency_code."#".$code ?></h1>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" onclick="refresh();">Refresh</button>
            </div>
        </div>
      </div>
    </div>

    <div style="position: absolute;bottom: 30px;left: 20px;text-align:center">
      <img src="<?= $file?>" alt="" style="height: 120px">
      <p style="color:#fff;font-size:18px;margin-top:5px;">QR code สำหรับรับบัตรคิวผ่าน App Mobile</p>
    </div>

    <?php include "../../inc/footer.php"; ?>
    <?php include "../../inc/js.php"; ?>
    <script src="js/main.js"></script>
  </body>
</html>

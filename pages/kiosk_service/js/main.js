$(function () {
  getTime();
  checkkiosk();
  setInterval(function(){getTime()},1000);

})


 // $('#myModalService').modal('toggle');
 function checkkiosk()
 {
    var kiosk_code = $('#kiosk_code').val();
    if(kiosk_code == "")
    {
      $('#myModalKiosk').modal('toggle');
    }
 }

function getTime()
{
   $.post("service/gettime.php")
     .done(function( data ) {
       $('#timeNow').html(data);
       checkkioskOpen(data);
       checkBreak(data);
   });
}


function refresh()
{
    location.reload();
}

function checkkioskOpen(time)
{
      var start_time  = $('#service_open').val() + ":00";
      var end_time    = $('#service_close').val() + ":00";

      //console.log(start_time + " " + end_time);

      var timeStart = new Date("01/01/2007 " + start_time);
      var timeEnd   = new Date("01/01/2007 " + end_time);
      var timeNow   = new Date("01/01/2007 " + time);

      var startDiff = timeStart - timeNow;
      var endDiff   = timeEnd   - timeNow;
      //console.log(startDiff);
      //console.log(endDiff);

      if(startDiff > 0 && endDiff > 0 && !$("#myModalServiceOpen").data('bs.modal')?.isShown)
      {
        $('#myModalServiceOpen').modal('toggle');
      }

      if(startDiff < 0 && endDiff > 0  && $("#myModalServiceOpen").data('bs.modal')?.isShown)
      {
        $('#myModalServiceOpen').modal('toggle');
      }

      if(endDiff < 0 && !$("#myModalServiceOpen").data('bs.modal')?.isShown)
      {
        $('#myModalServiceOpen').modal('toggle');
      }
}

function checkBreak(time)
{
  $(".break").each(function(index) {
      var value = $( this ).val();
      if(value != "")
      {
        var myArr = value.split("|");
        var start_time  = myArr[0] + ":00";
        var end_time    = myArr[1] + ":00";
        //console.log(start_time + " " + end_time);

        var timeStart = new Date("01/01/2007 " + start_time);
        var timeEnd   = new Date("01/01/2007 " + end_time);
        var timeNow   = new Date("01/01/2007 " + time);

        var startDiff = timeStart - timeNow;
        var endDiff   = timeEnd   - timeNow;
        //console.log(startDiff);
        //console.log(endDiff);

        if(startDiff < 0  && endDiff > 0 && !$("#myModalService").data('bs.modal')?.isShown)
        {
          var showTime = "เวลา " + myArr[0] + " - " + myArr[1] + " น.";
          $('#timeBreak').html(showTime);
          $('#myModalService').modal('toggle');


        }

        if(endDiff < 0 && $("#myModalService").data('bs.modal')?.isShown)
        {
          $('#myModalService').modal('toggle');
          $( this ).val("");
        }

      }

    });
}

function closeApp()
{
  // boundAsync.closeApp();
}

function openModal(){
  $('#myModal').modal('toggle');
  $('#myModal').on('shown.bs.modal', function() {
    $('#user').focus();
  });
}

$('#user').on('blur', function(event) {
  event.preventDefault();
  $('#user').focus();
});

function openModal2(){
  $('#myModal').modal('toggle');
}

<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$transQueueId      = isset($_POST['transQueueId'])?$_POST['transQueueId']:"";
$refCode          = isset($_POST['refCode'])?$_POST['refCode']:"";


$sql   = "SELECT * FROM t_trans_queue where trans_queue_id = '$transQueueId' and status_queue in ('W','R') ";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$dataCount  = $json['dataCount'];
$data       = $json['data'];

if($dataCount > 0)
{
  $status_queue = $data[0]['status_queue'];
  $arrUpdate['trans_queue_id'] = $transQueueId;
  if($status_queue == "R")
  {
    $arrUpdate['status_queue']   = 'S';
  }else{
    $arrUpdate['status_queue']   = $status_queue;
  }

  $sql = DBUpdatePOST($arrUpdate,'t_trans_queue','trans_queue_id');

  $query      = DbQuery($sql,null);
  $row        = json_decode($query, true);
  $errorInfo  = $row['errorInfo'];

  if(intval($errorInfo[0]) == 0){
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail')));
  }
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}




?>

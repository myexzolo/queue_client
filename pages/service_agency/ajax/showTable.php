<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px">ลำดับ</th>
      <th style="width:50px">รหัสงาน</th>
      <th>ชื่องานบริการ (ภาษาไทย)</th>
      <th>ชื่องานบริการ (ภาษาอังกฤษ)</th>
      <th style="width:90px">เวลามาตราฐาน</th>
      <th >หมวดงาน</th>
      <th style="width:80px">จอง Online</th>
      <th style="width:80px">สถานะ</th>
      <th style="width:40px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:40px;">แก้ไข</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $agencyCode = $_SESSION['AGENCY_CODE'];
      
      $sqls   = "SELECT *
                 FROM t_service_agency
                 where is_active not in ('D') and agency_code = '$agencyCode'
                 ORDER BY cat_service_id , service_code_a";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0)
      {
        foreach ($rows as $key => $value) {
          $is_active        = $value['is_active'];
          $reserve_status   = $value['reserve_status_a'];
          // $mem_username     = $value['mem_username']?;

          $activeTxt  = "";
          $bg         = "";
          $onclick    = "";
          $disabled   = "";

          $activeTxtR  = "";
          $bgR         = "";



          if($is_active == "Y")
          {
            $activeTxt = "ใช้งาน";
            $bg        = "bg-green-active";
          }else if($is_active == "N"){
            $activeTxt = "ไม่ใช้งาน";
            $bg        = "bg-gray";
          }

          if($reserve_status == "Y")
          {
            $activeTxtR = "เปิดจอง";
            $bgR        = "bg-green-active";
          }else if($reserve_status == "N"){
            $activeTxtR = "ไม่เปิดจอง";
            $bgR        = "bg-gray";
          }

    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="center"><?=$value['service_code_a']?></td>
      <td align="left"><?=$value['service_name_a']?></td>
      <td align="left"><?=$value['service_name_en_a']?></td>
      <td align="right"><?=$value['kpi_time_a']." นาที"?></td>
      <td align="left"><?=getCatServiceName($value['cat_service_id'])?></td>
      <td><div class="<?=$bgR?>"><?=$activeTxtR ?></div></td>
      <td><div class="<?=$bg?>"><?=$activeTxt ?></div></td>
      <td>
        <a class="btn_point text-green"><i class="fa fa-eye" onclick="showForm('VIEW','<?=$value['service_agency_id']?>')"></i></a>
      </td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['service_agency_id']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>

$(function () {
  showDisplay();
});

function showDisplay()
{
  var limit = $('#limit').val();
  var serviceIdList = $('#serviceIdList').val();
  var agency_code = $('#agency_code').val();
  var numberCode = $('#numberCode').val();
  var point_id = $('#point_id').val();
  var ip = $('#ip').val();

  $.post("ajax/showDisplay.php",{limit:limit,serviceIdList:serviceIdList,agency_code:agency_code,point_id:point_id,numberCode:numberCode})
    .done(function( data ) {
      var numberCodeTmp  = data.queue_code;
      var serviceChannel = $('#serviceChannel').val();
      //console.log(numberCodeTmp + " ," + numberCode + " ," + data.status_queue);
      if(numberCodeTmp == "")
      {
         $('#show-display').html(data.html);
         setTimeout(function(){showDisplay();}, 1000);
      }
      else if(numberCodeTmp != numberCode || (data.status_queue == "R" && numberCodeTmp == numberCode))
      {
        $('#show-display').html(data.html);
        $('#numberCode').val(numberCodeTmp);
        //console.log(data);
        setTimeout(function(){showDisplay();}, 1000);
      }else{
        setTimeout(function(){showDisplay();}, 1000);
      }
  }).fail(function (jqXHR, textStatus, errorThrown) {
      //setTimeout(function(){showDisplay();}, 1000);
  });
}

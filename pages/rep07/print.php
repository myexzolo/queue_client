  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';



  $startDate  = $_POST['startDate'];
  $endDate    = $_POST['endDate'];
  $agencyStr  = $_POST['agencyStr'];
  $serviceStr = $_POST['serviceStr'];

  $startDateStr = DatetoThaiMonth($startDate);
  $endDateStr   = DatetoThaiMonth($endDate);

  $textDate = "";
  if($startDate == $endDate){
    $textDate = "วันที่ ".$startDateStr;
  }else{
    $textDate = "ตั้งแต่วันที่ ".$startDateStr." - ".$endDateStr;
  }

  $startDate  = $_POST['startDate']." 00:00:01";
  $endDate    = $_POST['endDate']." 23:59:59";

  $con = "";
  if($agencyStr != ""){
    $con = " and q.agency_code = '$agencyStr'";
  }

  $str = "ทุกประเภทบริการ";

  if($serviceStr != ""){
    $arrService = explode(":", $serviceStr);
    $str  = $arrService[1];
    $con .= " and q.service_id = '$arrService[0]'";
  }


  $sql = "SELECT count(q.queue_code) as total ,q.service_id
          FROM t_trans_queue q
          WHERE q.date_start between '$startDate' and '$endDate' and q.status_queue = 'E' $con
          GROUP BY q.service_id";

  $querys     = DbQuery($sql,null);
  $json       = json_decode($querys, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $rowt       = $json['data'];

  $arrSericeNum = array();
  for($i = 0; $i < $dataCount; $i++)
  {
    $service_id =  $rowt[$i]['service_id'];
    $total      =  $rowt[$i]['total'];

    $arrSericeNum[$service_id] = $total;
  }

  //CEIL ปัดขึ้น
  //ROUND ปัดขึ้นเกิน 5
  //FLOOR ปัดลง

  //คำนวณ %  = (จำนวน ÷ จำนวนทั้งหมด) x 100    ex (1/20)x100 = 5%.

  $sql = "SELECT CEIL(TIMESTAMPDIFF(SECOND,q.date_start,q.date_end)/60) as time_trans ,s.service_name_a , count(q.queue_code) as num ,s.service_id
          FROM t_trans_queue q, t_agency a, t_service_agency s
          WHERE q.date_start between '$startDate' and '$endDate' and status_queue = 'E'
          and q.agency_code = a.agency_code and q.agency_code = s.agency_code and q.service_id = s.service_id $con
          GROUP BY q.service_id, time_trans
          ORDER BY q.agency_code,q.service_id,time_trans desc";

  //echo $sql;

  $querys     = DbQuery($sql,null);
  $json       = json_decode($querys, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $rows       = $json['data'];

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'mode' => 'utf-8',
      'format' => 'A4',
      'fontDir' => array_merge($fontDirs, [
           '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'kanits' => [
              'R' => 'CSChatThai.ttf'
          ]
      ],
      'default_font' => 'kanits'
  ]);
  ob_start();
  // print_r($result);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
    <htmlpageheader name="firstpageheader">
      <div class="header" >
         <table width = "100%">
          <tr class="text-center">
              <td style="text-align:left;width:50px;"><img style="height:40px" src="../../image/logo.png"/></td>
              <td style="text-align:left;"><p>ระบบสนับสุนนการให้บริการ (Queue)</p></td>
              <td style="text-align:right;width:30%"><p>page {PAGENO} of {nb}</p></td>
           </tr>
         </table>
        </div>
    </htmlpageheader>
    <sethtmlpageheader name="firstpageheader" value="on" show-this-page="1" />
      <br>
      <div align="center" style="font-size:24px;font-weight:600">รายงานสรุปเวลาให้บริการแยกตามประเภทบริการ</div>
      <div align="center" style="font-size:24px;font-weight:600"><?= $str ?></div>
      <div align="center" style="font-size:22px;font-weight:600"><?=$textDate?></div><br>

    <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; overflow: wrap;" width = "100%">
    <thead>
      <tr class="text-center">
        <th class="thStyle" style="width:80px;">ลำดับที่</th>
        <th class="thStyle">เวลาต่อรายการ</th>
        <th class="thStyle" style="width:160px;">จำนวน (คน)</th>
        <th class="thStyle" style="width:160px;">เปอร์เซ็นต์</th>
      </tr>
    </thead>
    <tbody>
      <?php
          $numShow = 0;
          $serviceIdTmp = "";
          for($i = 0; $i < $dataCount; $i++)
          {
            $service_name    = $rows[$i]['service_name_a'];
            $service_id      = $rows[$i]['service_id'];
            $time_trans      = $rows[$i]['time_trans'];
            $num             = $rows[$i]['num'];
            $timeTrans       = gmdate("H:i", ($time_trans*60));

            $persent         = number_format(($num/$arrSericeNum[$service_id])*100);

            $flag = "";

            if($serviceIdTmp != $service_id)
            {
              $numShow = 0;
              $flag = "Y";

              if($serviceIdTmp != "" && $i > 0)
              {
                echo "
                    </tbody>
                  </table>
                  <div class=\"break\"></div>
                  <table border=\"1\" cellspacing=\"0\" style=\"border-collapse:collapse; border:solid #333 1px; overflow: wrap;\" width = \"100%\">
                  <thead>
                    <tr class=\"text-center\">
                    <th class=\"thStyle\" style=\"width:80px;\">ลำดับที่</th>
                    <th class=\"thStyle\">เวลาต่อรายการ</th>
                    <th class=\"thStyle\" style=\"width:145px;\">จำนวน (คน)</th>
                    <th class=\"thStyle\" style=\"width:145px;\">เปอร์เซ็นต์</th>
                    </tr>
                  </thead>
                  <tbody>
                ";
              }
              $serviceIdTmp = $service_id;
            }


            $numShow++;

            if($flag == 'Y')
            {
              echo "
              <tr class=\"text-center\">
                <td class=\"content\" colspan=\"4\" style=\"text-align:left;\">$service_name</td>
              </tr>
              ";
            }
            ?>
            <tr class="text-center">
              <td class="content" style="text-align:center;"><?=$numShow;?></td>
              <td class="content" style="text-align:center;"><?= $timeTrans ?></td>
              <td class="content" style="text-align:right;"><?= $num ?></td>
              <td class="content" style="text-align:right;"><?= $persent."%" ?></td>
            </tr>
          <?php
          }
        ?>
      </tbody>
    </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <table width = "100%">
                <tr class="text-center">
                    <td style="text-align:left;width:50%"><p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'</td>

                 </tr>
               </table>
              </div>';

   $header  = '<div class="header" >
                <table width = "100%">
                 <tr class="text-center">
                     <td style="text-align:left;width:50px;"><img style="height:40px" src="../../image/logo.png"/></td>
                     <td style="text-align:left;"><p>ระบบสนับสุนนการให้บริการ (Queue)</p></td>
                     <td style="text-align:right;width:30%"><p>page {PAGENO} of {nb}</p></td>
                  </tr>
                </table>
               </div>';


    $html = ob_get_contents();
    ob_end_clean();
    ob_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('รายงานสรุปเวลาให้บริการแยกตามประเภทบริการ');
    // $mpdf->StartProgressBarOutput(2);
    // $mpdf->AddPage('P','','','','',left,right,top,10,10,10);
    // $mpdf->AddPage('P','','','','',10,10,12,10,10,10);
    $mpdf->AddPage('P','','','','',10,10,20,15,5,5);
    $mpdf->SetHTMLHeader($header);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('rep07.pdf', \Mpdf\Output\Destination::INLINE);
    // $mpdf->Output('rep01.pdf', \Mpdf\Output\Destination::FILE);
  ?>

<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
th{
  vertical-align: middle !important;
}
</style>
<?php
$startDate  = $_POST['startDate'];
$endDate    = $_POST['endDate'];
$agencyStr  = $_POST['agencyStr'];
$serviceStr = $_POST['serviceStr'];

$startDateStr = DatetoThaiMonth($startDate);
$endDateStr   = DatetoThaiMonth($endDate);

$textDate = "";
if($startDate == $endDate){
  $textDate = "วันที่ ".$startDateStr;
}else{
  $textDate = "ตั้งแต่วันที่ ".$startDateStr." - ".$endDateStr;
}

$startDate  = $_POST['startDate']." 00:00:01";
$endDate    = $_POST['endDate']." 23:59:59";

$con = "";
if($agencyStr != ""){
  $con = " and q.agency_code = '$agencyStr'";
}

$str = "ทุกประเภทบริการ";

if($serviceStr != ""){
  $arrService = explode(":", $serviceStr);
  $str  = $arrService[1];
  $con .= " and q.service_id = '$arrService[0]'";
}

$sql = "SELECT count(q.queue_code) as total ,q.service_id
        FROM t_trans_queue q
        WHERE q.date_start between '$startDate' and '$endDate' and q.status_queue = 'E' $con
        GROUP BY q.service_id";

$querys     = DbQuery($sql,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rowt       = $json['data'];

$arrSericeNum = array();
for($i = 0; $i < $dataCount; $i++)
{
  $service_id =  $rowt[$i]['service_id'];
  $total      =  $rowt[$i]['total'];

  $arrSericeNum[$service_id] = $total;
}

//CEIL ปัดขึ้น
//ROUND ปัดขึ้นเกิน 5
//FLOOR ปัดลง

//คำนวณ %  = (จำนวน ÷ จำนวนทั้งหมด) x 100    ex (1/20)x100 = 5%.

$sql = "SELECT CEIL(TIMESTAMPDIFF(SECOND,q.date_start,q.date_end)/60) as time_trans ,s.service_name_a , count(q.queue_code) as num ,s.service_id
        FROM t_trans_queue q, t_agency a, t_service_agency s
        WHERE q.date_start between '$startDate' and '$endDate' and status_queue = 'E'
        and q.agency_code = a.agency_code and q.agency_code = s.agency_code and q.service_id = s.service_id $con
        GROUP BY q.service_id, time_trans
        ORDER BY q.agency_code,q.service_id,time_trans desc";

//echo $sql;

$querys     = DbQuery($sql,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];

?>
<div class="box-body">
<div align="center">
  <p style="font-size:24px;font-weight:600">รายงานสรุปเวลาให้บริการแยกตามประเภทบริการ</p>
  <p style="font-size:24px;font-weight:600"><?= $str ?></p>
  <p style="font-size:22px;font-weight:600"><?=$textDate?></p>
</div>
<input type="hidden" id="dataCount" value="<?=$dataCount?>">
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px;">ลำดับที่</th>
      <th >งานบริการ</th>
      <th >เวลาต่อรายการ</th>
      <th >จำนวน (คน)</th>
      <th >เปอร์เซ็นต์</th>
    </tr>
  </thead>
<tbody>
  <?php
      $numShow = 0;
      $serviceIdTmp = "";
      for($i = 0; $i < $dataCount; $i++)
      {
          $service_name    = $rows[$i]['service_name_a'];
          $service_id      = $rows[$i]['service_id'];
          $time_trans      = $rows[$i]['time_trans'];
          $num             = $rows[$i]['num'];
          $timeTrans       = gmdate("H:i", ($time_trans*60));

          $persent         = number_format(($num/$arrSericeNum[$service_id])*100);

          if($serviceIdTmp != $service_id)
          {
            $numShow = 0;
            $serviceIdTmp = $service_id;
          }
          $numShow++;

      ?>
        <tr class="text-center">
          <td ><?=$numShow;?></td>
          <td style="text-align:center;"><?= $timeTrans ?></td>
          <td style="text-align:left;"><?= $service_name ?></td>
          <td style="text-align:right;"><?= $num ?></td>
          <td style="text-align:right;"><?= $persent."%" ?></td>
        </tr>
      <?php
      }
    ?>
  </tbody>
</table>
</div>
<div class="box-footer">
  <button type="button" class="btn btn-success btn-flat pull-right btn-rep" onclick="printPdf()" >พิมพ์</button>
</div>
<script>
  $(function () {
    var groupColumn = 2;

    $('#tableDisplay').DataTable({
      'paging'        : true,
      'lengthChange'  : false,
      'searching'     : false,
      'ordering'      : false,
      'info'          : true,
      'autoWidth'     : false,
      'columnDefs'    : [{ "visible": false, "targets": groupColumn }],
      'order'         : [[ groupColumn, 'asc' ]],
      'drawCallback'  : function ( settings ) {
                        var api = this.api();
                        var rows = api.rows( {page:'current'} ).nodes();
                        var last=null;

                        api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                          if ( last !== group )
                          {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="6"><b>'+group+'</b></td></tr>'
                            );
                            last = group;
                          }
                      } );
                      }
    })

  })
</script>

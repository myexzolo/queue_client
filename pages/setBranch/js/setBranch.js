// JS Login //
showForm('SET');

function showForm(action){
  $.post("ajax/form.php",{action:action})
    .done(function( data ) {
      $('#action').remove();
      $('#show-form').html(data);
  });
}


$('#form').on('submit', function(event) {
  event.preventDefault();
  // $('.icons').remove('span');
  if ($('#form').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      // console.log(data);
      // $.smkProgressBar({
      //   element:'body',
      //   status:'start',
      //   bgColor: '#000',
      //   barColor: '#fff',
      //   content: 'Loading...'
      // });
      // setTimeout(function(){
        // $.smkProgressBar({status:'end'});
        //console.log(data);
        $('#form').smkClear();
        $.smkAlert({text: data.message,type: data.status});
        if(data.status == 'success'){
          window.location = '../../pages/login/';
        }
      // }, 1000);
    }).fail(function (jqXHR, textStatus) {
        console.log(jqXHR);
    });
  }
});

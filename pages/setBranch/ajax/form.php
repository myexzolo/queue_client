<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$disabled = '';
$type = 'text';
$icons = 'fa fa-building';
$placeholder1 = 'รหัสสาขา';
$placeholder2 = 'IP Queue Center';


$sql = "SELECT * FROM t_agency where agency_id = '1'";

$query  = DbQuery($sql,null);
$json   = json_decode($query, true);
$count  = $json['dataCount'];
$rows   = $json['data'];

if($count > 0)
{
  $agency_code = $rows[0]['agency_code'];
  $ip_center   = $rows[0]['ip_center'];
}


?>


<input type="hidden" name="action" value="<?=$action?>">
<div class="form-group has-feedback">
  <label>รหัสสาขา</label>
  <input type="text" value="<?= @$agency_code ?>" id="agency_code" name="agency_code" data-smk-msg="กรุณากรอกรหัสสาขา"  class="form-control" placeholder="<?=$placeholder1?>" required style="font-size: 22px">
</div>
<div class="form-group has-feedback">
  <label>IP Queue Center Service</label>
  <input type="text" value="<?= @$ip_center ?>" id="ip_center" name="ip_center" data-smk-msg="กรุณากรอก IP Queue Center" class="form-control" placeholder="<?=$placeholder2?>" <?=$disabled?> required style="font-size: 22px">
</div>

<div class="row">
  <!-- /.col -->
  <div class="col-md-12">
    <button type="submit" id="sendForm" class="btn btn-flat" <?=$disabled?>>บันทึก</button>
  </div>
  <!-- <div class="col-md-12">
    <a href="../forget_pw" class="pull-right" style="margin-top:5px;">ลืมรหัสผ่าน</a>
  </div> -->
  <!-- /.col -->
</div>

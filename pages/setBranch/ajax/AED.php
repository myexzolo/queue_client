<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$agency_code  = isset($_POST['agency_code'])?$_POST['agency_code']:"";
$ip_center    = isset($_POST['ip_center'])?$_POST['ip_center']:"";



///////////////// Service ////////////////////////////
$ipAgency    = $ip_center;
$data_array  = array(
                   "functionName" => "getAgency",  ///แก้ ชื่อ Service
                   "dataJson" => $_POST,
                 );

$url        = "http://$ipAgency/ws/service.php";
//echo $url;
//echo json_encode($data_array);

$make_call = callAPI('POST', $url, json_encode($data_array));
$response = json_decode($make_call, true);
$status   = $response['status'];
$data     = $response['data'];


$data_array  = array(
                   "functionName" => "getUser",  ///แก้ ชื่อ Service
                   "dataJson" => $_POST,
                 );

$make_call_user  = callAPI('POST', $url, json_encode($data_array));
$responseUser    = json_decode($make_call_user, true);
$statusUser      = $responseUser['status'];
$dataUser        = $responseUser['data'];

//print_r($dataUser);


$data_array  = array(
                   "functionName" => "getService",  ///แก้ ชื่อ Service
                   "dataJson" => $_POST,
                 );

$make_call_service  = callAPI('POST', $url, json_encode($data_array));
$responseService    = json_decode($make_call_service, true);
$statusService      = $responseService['status'];
$dataService        = $responseService['data'];



$data_array  = array(
                   "functionName" => "getRole",  ///แก้ ชื่อ Service
                   "dataJson" => $_POST,
                 );

$make_call_role = callAPI('POST', $url, json_encode($data_array));
$responseRole       = json_decode($make_call_role, true);
$statusRole         = $responseRole['status'];
$dataRole           = $responseRole['data'];


///////////////////////////////////////////////////////////////////


if($status == "200" && isset($data['data'][0]))
{

  $sql = "SELECT * FROM t_agency";
  $query  = DbQuery($sql,null);
  $json   = json_decode($query, true);
  $count  = $json['dataCount'];

  $dataAgency = $data['data'][0];

  $dataAgency["agency_id"] = '1';
  $dataAgency["ip_center"] = $ip_center;


  $dataAgency  = chkDataEmpty($dataAgency);

  if($count > 0){
    $sql = DBUpdatePOST($dataAgency,'t_agency','agency_id');
  }else{
    $sql = DBInsertPOST($dataAgency,'t_agency');
  }

  ///---------------  set user ---------------------------------///

  if($statusUser == "200" && isset($dataUser['data'][0]))
  {
    $dataUserArr = $dataUser['data'];
    for($x=0; $x<count($dataUserArr) ; $x++)
    {
        $dateUser = $dataUserArr[$x];
        $user_id_center = $dateUser['user_id'];

        $sqlu = "SELECT * FROM t_user where user_id_center = '$user_id_center'";
        $query  = DbQuery($sqlu,null);
        $jsonu   = json_decode($query, true);
        $countu  = $jsonu['dataCount'];

        unset($dateUser['user_id']);
        $dateUser['user_id_center'] = $user_id_center;

        $dateUser = chkDataEmpty($dateUser);

        $dataJson['agency_code']  = $agency_code;
        $dataJson['table_name']   = 't_user';
        $dataJson['name_id']      = 'user_id';
        $dataJson['id']           = $user_id_center;
        $dataJson['value']        = 'S';

        $data_array  = array(
                           "functionName" => "setStatusSend",  ///แก้ ชื่อ Service
                           "dataJson" => $dataJson,
                         );

        //print_r(json_encode($data_array));

        $make_call  = callAPI('POST', $url, json_encode($data_array));
        $response   = json_decode($make_call, true);
        $statusU    = $response['status'];

        if($statusU == '200')
        {
          $dateUser['status_send'] = "S";
        }else{
          $dateUser['status_send'] = "N";
        }

        if($countu > 0)
        {
          $sql .= DBUpdatePOST($dateUser,'t_user','user_id_center');
        }else{
          $sql .= DBInsertPOST($dateUser,'t_user');
        }
    }
  }

  if($statusService == "200" && isset($dataService['data'][0]))
  {
    $dataServiceArr = $dataService['data'];
    $sqlt   = "TRUNCATE TABLE t_service;";
    DbQuery($sqlt,null);
    for($x=0; $x<count($dataServiceArr) ; $x++)
    {
        $dateService    = $dataServiceArr[$x];
        $sql .= DBInsertPOST($dateService,'t_service');
    }
  }


  if($statusRole == "200" && isset($dataRole['data'][0]))
  {
    $dataRoleArr = $dataRole['data'];
    $sqlt   = "TRUNCATE TABLE t_role;";
    DbQuery($sqlt,null);
    for($x=0; $x<count($dataRoleArr) ; $x++)
    {
        $dateRole   = $dataRoleArr[$x];

        $dateRole  = chkDataEmpty($dateRole);

        $sql .= DBInsertPOST($dateRole,'t_role');
    }
  }

  //echo $sql;

  $query      = DbQuery($sql,null);
  $row        = json_decode($query, true);
  $errorInfo  = $row['errorInfo'];

  if(intval($errorInfo[0]) == 0){
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail ไม่พบ Web Service link :'.$url)));
  }
}
else
{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>

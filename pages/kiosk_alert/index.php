<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../image/logo.png">
    <title>Home : สำนักงานประกันสังคม</title>

    <?php include "../../inc/css.php"; ?>
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body class="main-bg">
    <?php
    include "../../inc/nav.php";
    $agency_code  = @$_GET['acode'];
    $kiosk_code   = @$_GET['kcode'];
    $fontSize     = @$_GET['fsize'];
    ?>
    <div class="box-queue-run">
      <div style="font-size:<?=$fontSize.'px' ?>;color:#fff;width:100%;text-align:center;margin-top:calc(40vh)">
          <?php

              $sql   = "SELECT * FROM t_kiosk where is_active = 'Y' and agency_code  = '$agency_code' and kiosk_code = '$kiosk_code'";
              $query = DbQuery($sql,null);
              $json  = json_decode($query,true);
              $rows  = $json['data'][0];

              $text_alert  = $rows['text_alert'];

              echo $text_alert;
          ?>
     </div>
  </div>
  </body>
</html>
<script>
  setInterval(function(){location.reload();},30000);
</script>

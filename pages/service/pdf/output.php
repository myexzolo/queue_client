
    <?php
    // session_start();
    // include('../../../inc/function/session.php');
    include('../../../inc/function/connect.php');
    // include('../../../admin/inc/function/mainFunc.php');
    header("Content-type:text/html; charset=UTF-8");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    require_once '../../../mpdf2/vendor/autoload.php';



    $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
    $fontDirs = $defaultConfig['fontDir'];

    $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
    $fontData = $defaultFontConfig['fontdata'];

    $mpdf = new \Mpdf\Mpdf([
        'mode' => 'utf-8',
        'format' => 'A4',
        'fontDir' => array_merge($fontDirs, [
             __DIR__ . '/font',
        ]),
        'fontdata' => $fontData + [
            'kanits' => [
                'R' => 'CSChatThai.ttf'
            ]
        ],
        'default_font' => 'kanits'
    ]);

    ob_start();

    ?>

    <?php
        $sql = "SELECT * FROM t_trans_queue WHERE trans_queue_id = '{$_POST['id']}'";
        $query = DbQuery($sql,null);
        $json = json_decode($query,true)['data'][0];

    ?>

    <div class="pdf-box">
      <img class="pdf-img" src="../../../image/logo.png" alt="">
      <h1 class="pdf-h"><?=$json['queue_code']?></h1>
      <h1 class="pdf-q">มีคิวก่อนหน้าท่าน 2 ราย</h1>
      <h1 class="pdf-q"><?=$json['date_start']?></h1>
    </div>


<?php

  $html = ob_get_contents();
  ob_end_clean();
  $stylesheet = file_get_contents('../css/main.css');
  $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
  $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

  $random = 'PDF_'.time().rand(0,1000);

  if(!$mpdf->Output("../output/$random.pdf", \Mpdf\Output\Destination::FILE)){
    header("Content-Type: application/json");
    exit(json_encode(array("status" => "success","message" => "success",'pdf_name' => $random.'.pdf')));
  }else{
    header("Content-Type: application/json");
    exit(json_encode(array("status" => "danger","message" => "print PDF Fail")));
  }

?>

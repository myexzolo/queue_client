<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../image/logo.png">
    <title>Home : สำนักงานประกันสังคม</title>

    <?php include "../../inc/css.php"; ?>
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body class="main-bg">
    <?php include "../../inc/nav.php"; ?>

    <div class="box-queue-run">

        <div class="row">
          <?php
              $sql = "SELECT * FROM t_service WHERE service_id IN({$_GET['service_id']}) AND is_active = 'Y'";
              $query = DbQuery($sql,null);
              $json = json_decode($query,true);
              foreach ($json['data'] as $value) {
          ?>
          <div class="col-xs-6">
            <div class="btn-border" onclick="goSave('<?=$value['service_code']?>','<?=$_GET['agency_code']?>','<?=$value['service_id']?>')">
              <h1><?=$value['service_name']?></h1>
            </div>
          </div>
          <?php } ?>
          <div class="col-xs-6">
            <div class="btn-border" onclick="goback('../home/')">
              <h1>Home</h1>
            </div>
          </div>
        </div>


    </div>

    <?php include "../../inc/footer.php"; ?>
    <?php include "../../inc/js.php"; ?>
    <script src="js/main.js"></script>
  </body>
</html>

// $('#formLogin').on('submit', function(event) {
//   event.preventDefault();
//
//   if ($('#formLogin').smkValidate()) {
//     // Code here
//     $.smkAlert({
//       text: 'Validate!',
//       type: 'success'
//     });
//   }
//
// });

function openModal(){
  $('#myModal').modal('toggle');
}

function goSave(id,agency_code,service_id){
  $.post("service/saveQueue.php",{id:id,agency_code:agency_code,service_id:service_id})
    .done(function( data ) {

      if(data.status == 'success'){
        $.post("pdf/output.php",{id:data.id})
          .done(function( data ) {
            $.smkAlert({text: data.message,type: data.status});
            //alert(data.pdf_name);

            var path = "C:\\xampp\\htdocs\\queue_client\\page\\service\\output\\" + data.pdf_name;
            //console.log(path);
            $.get("http://localhost:20001/ws/printQueue/"+path)
              .done(function( data ) {
                window.location = "../home";
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                window.location = "../home";
            });
        });
      }else{
        $.smkAlert({text: data.message,type: data.status});
      }

  });
}

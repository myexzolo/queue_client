<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../image/logo.png">
    <title>Home : สำนักงานประกันสังคม</title>

    <?php include "../../inc/css.php"; ?>
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body class="main-bg">
    <?php include "../../inc/nav.php"; ?>

    <div class="box-queue-run">

      <table class="table ">
        <thead>
          <tr class="box-message-head">
            <td>ช่องบริการ</td>
            <td>หมายเลขคิว</td>
          </tr>
        </thead>
        <tbody>
          <?php for ($i=1; $i <=5 ; $i++) {?>
            <tr class="box-message-head">
              <td><?=$i?></td>
              <td>R000<?=$i?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>


    </div>

    <?php include "../../inc/footer.php"; ?>
    <?php include "../../inc/js.php"; ?>
    <script src="js/main.js"></script>
  </body>
</html>

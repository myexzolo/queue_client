<?php

$dir = dirname(__FILE__) . DIRECTORY_SEPARATOR;
$dir = str_replace("\batch","",$dir);
include($dir.'inc\function\mainFunc.php');
include($dir.'inc\function\connect.php');

/// --------------- Log Send Service ----------------------///
echo "=============== Start =============\r\n\r\n";
while (true)
{
  $sqls   = "SELECT * FROM t_log_send_service where status_send = 'N'";
  $querys     = DbQuery($sqls,null);
  $json       = json_decode($querys, true);
  $dataCount  = $json['dataCount'];
  $rows       = $json['data'];

  for($i=0; $i < $dataCount; $i++)
  {
    $id_log_send = $rows[$i]['id_log_send'];
    $url         = $rows[$i]['url'];
    $data        = $rows[$i]['data'];
    $table_name  = $rows[$i]['table_name'];
    $id_update   = $rows[$i]['id_update'];
    $data_update = $rows[$i]['data_update'];

    //echo $url;

    $make_call_user  = callAPI('POST', $url, $data);
    $responseUser    = json_decode($make_call_user, true);
    $statusUser      = $responseUser['status'];

    $sql = "";
    //echo $statusUser;
    if($statusUser == "200")
    {
        $data_update = json_decode($data_update, true);

        $arrUpdate['id_log_send']     =  $id_log_send;
        $arrUpdate['status_send']     =  "S";
        $sql  = DBUpdatePOST($data_update,$table_name,$id_update);
        $sql .= DBUpdatePOST($arrUpdate,'t_log_send_service','id_log_send');

        //echo $sql;
        DbQuery($sql,null);
        echo "=========== Send Data =============\r\n";
        echo "table :".$table_name."\r\n";
        echo "data :".json_encode($data_update)."\r\n";
    }
    //echo  $sql;
  }
  sleep(60);//วินาที
}

?>

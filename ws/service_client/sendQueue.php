<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');

    // $status   = 200;
    // $message  = 'success';
    $dataJson     = isset($request['dataJson'])?$request['dataJson']:"";

    unset($request['type']);
    unset($request['functionName']);
    if($dataJson != "")
    {
      // print_r($dataJson);
      $trans_queue_id          = $dataJson['qid'];
      $transferServiceId       = $dataJson['tsid'];
      $user_ref_code           = $dataJson['userRefCode'];

      $arrUpdate['trans_queue_id']  = $trans_queue_id;
      $arrUpdate['status_queue']    = 'E';
      $arrUpdate['user_ref_code']   = $user_ref_code;
      $arrUpdate['date_end']        = date('Y-m-d H:i:s');

      $sql = DBUpdatePOST($arrUpdate,'t_trans_queue','trans_queue_id');

      //echo   $sql;
      $query      = DbQuery($sql,null);
      $row        = json_decode($query, true);
      $errorInfo  = $row['errorInfo'];


      if(intval($row['errorInfo'][0]) == 0){

        $sqls       = "SELECT * FROM t_trans_queue where trans_queue_id = '$trans_queue_id'"; //เปลี่ยน table
        $querys     = DbQuery($sqls,null);
        $json       = json_decode($querys, true);
        $rows       = $json['data'][0];

        $agency_code   = $rows["agency_code"];
        
        ///// ------------- Update Status Service Channel ------------------///
        $service_channel = $rows['service_channel'];
        $point_id        = $rows['point_id'];
        $sqlch    = "UPDATE t_service_channel set status_active = 'S' where service_channel = '$service_channel' and point_id = '$point_id';";
        DbQuery($sqlch,null);

        /////--------------------------------------------------------------///

        $rows["action"]   = "U";

        $rows = chkDataEmpty($rows);

        $sql        = "SELECT * FROM t_agency WHERE agency_code = '$agency_code'";
        $query      = DbQuery($sql,null);
        $agency     = json_decode($query,true)['data'][0];

        $ipAgency = $agency['ip_center'];

        $data_array  = array(
                           "functionName" => "manageTransQueue",  ///แก้ ชื่อ Service
                           "dataJson" => $rows,
                         );
        $url        = "http://$ipAgency/ws/service.php";

        //echo json_encode($data_array);

        $make_call = callAPI('POST', $url, json_encode($data_array));
        $response = json_decode($make_call, true);
        $status   = $response['status'];
        $data     = $response['data'];

        $arrUpdate['trans_queue_id']     =  $trans_queue_id; //แก้ ID
        $arrUpdate['ref_code']           =  $rows["ref_code"];
        $arrUpdate['status_send']        =  "N";

        $sql = "";
        if($status == "200")
        {
            $arrUpdate['status_send']  =  "S";
        }else{
            $arrLog['url']  = $url;
            $arrLog['data'] = json_encode($data_array);
            $arrLog['table_name'] = 't_trans_queue';
            $arrLog['id_update']  = 'trans_queue_id';
            $arrLog['date_create']  = date('Y/m/d H:i:s');
            $arrLog['data_update']  = json_encode($arrUpdate);

            $sql = DBInsertPOST($arrLog,'t_log_send_service');
        }
        $sql .= DBUpdatePOST($arrUpdate,'t_trans_queue','trans_queue_id');
        //echo $sql;
        DbQuery($sql,null);


        /////////////////// tranfer ////////////////////////
        $sqlt       = "SELECT * FROM t_point_service where  find_in_set($transferServiceId,service_id_list) <> 0 and is_active = 'Y'"; //เปลี่ยน table
        $queryt     = DbQuery($sqlt,null);
        $jsont      = json_decode($queryt, true);
        $rowt       = $jsont['data'][0];

        $newQueue['queue_code']   = $rows['queue_code'];
        $newQueue['date_start']   = date('Y-m-d H:i:s');
        // $newQueue['ref_code']     = $agency_code."#A".$id;
        $newQueue['agency_code']  = $agency_code;
        $newQueue['service_id']   = $transferServiceId;
        $newQueue['date_create']  = date('Y-m-d H:i:s');
        $newQueue['type_service'] = "2";
        $newQueue['type_queue']   = "1";
        $newQueue['trans_ref_code'] = $rows["ref_code"];
        $newQueue['status_queue']   = "W";
        $newQueue['point_id']       = $rowt['point_id'];


        $sqls = DBInsertPOST($newQueue,'t_trans_queue');

        $query      = DbQuery($sqls,null);
        $row        = json_decode($query, true);
        $errorInfo  = $row['errorInfo'];

        //echo $sqls;

        if(intval($errorInfo[0]) == 0)
        {
            $trans_queue_id = $row['id'];
            $newQueue["action"]   = "ADD";
            $newQueue['ref_code'] = $agency_code."#A".$trans_queue_id;


            $data_array  = array(
                               "functionName" => "manageTransQueue",  ///แก้ ชื่อ Service
                               "dataJson" => $newQueue,
                             );
            $url        = "http://$ipAgency/ws/service.php";

            //echo json_encode($data_array);

            $make_call = callAPI('POST', $url, json_encode($data_array));
            $response = json_decode($make_call, true);
            $status   = $response['status'];
            $data     = $response['data'];

            $arrUpdate2['trans_queue_id']     =  $trans_queue_id; //แก้ ID
            $arrUpdate2['ref_code']           =  $newQueue["ref_code"];
            $arrUpdate2['status_send']        =  "N";

            $sql2 = "";
            if($status == "200")
            {
                $arrUpdate2['status_send']  =  "S";
            }else{
                $arrLog['url']  = $url;
                $arrLog['data'] = json_encode($data_array);
                $arrLog['table_name'] = 't_trans_queue';
                $arrLog['id_update']  = 'trans_queue_id';
                $arrLog['date_create']  = date('Y/m/d H:i:s');
                $arrLog['data_update']  = json_encode($arrUpdate2);

                $sql2 = DBInsertPOST($arrLog,'t_log_send_service');
            }
            $sql2 .= DBUpdatePOST($arrUpdate2,'t_trans_queue','trans_queue_id');
            //echo $sql;

            //echo $sql2;
            DbQuery($sql2,null);

            $status   = 200;
            $message  = 'success';

        }
        /////////////////////////////////////////////////////
      }
      // $data = $dataJson;
    }

?>

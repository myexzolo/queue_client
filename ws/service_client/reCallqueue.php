<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');

    // $status   = 200;
    // $message  = 'success';
    $dataJson     = isset($request['dataJson'])?$request['dataJson']:"";

    unset($request['type']);
    unset($request['functionName']);
    if($dataJson != "")
    {
      $trans_queue_id   = $dataJson['qid'];

      $arrUpdate['trans_queue_id']  = $trans_queue_id;
      $arrUpdate['status_queue']    = 'R';
      $arrUpdate['date_service']    = date('Y-m-d H:i:s');

      $sql = DBUpdatePOST($arrUpdate,'t_trans_queue','trans_queue_id');

      //echo   $sql;
      $query      = DbQuery($sql,null);
      $row        = json_decode($query, true);
      $errorInfo  = $row['errorInfo'];

      if(intval($errorInfo[0]) == 0){
        $status   = 200;
        $message  = 'success';
      }
    }
?>

<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');

    // $status   = 200;
    // $message  = 'success';
    $dataJson     = isset($request['dataJson'])?$request['dataJson']:"";

    unset($request['type']);
    unset($request['functionName']);
    if($dataJson != "")
    {
      // print_r($dataJson);
      $kiosk_code      = $dataJson['kioskCode'];
      $agency_code     = $dataJson['acode'];
      $status          = $dataJson['status'];

      $sql   = "SELECT k.kiosk_id,k.kiosk_code,k.service_open,k.service_close,k.kiosk_open,
                       k.kiosk_close,k.agency_code,k.status_kiosk,k.ref_code , p.ref_code as point_ref_code
                FROM  t_kiosk k, t_point_service p
                where k.kiosk_code = '$kiosk_code' and k.agency_code = '$agency_code' and k.point_id = p.point_id";

      $query     = DbQuery($sql,null);
      $json      = json_decode($query, true);
      $dataCount = $json['dataCount'];

      if($dataCount > 0)
      {
         $rowsKiosk   = $json['data'][0];
         $kiosk_id    = $rowsKiosk['kiosk_id'];
         $ref_code    = $rowsKiosk['ref_code'];
         $arrUpdate['kiosk_id']      = $kiosk_id;
         $arrUpdate['status_kiosk']  = $status;

         $sql = DBUpdatePOST($arrUpdate,'t_kiosk','kiosk_id');
         $query = DbQuery($sql,null);
         $json  = json_decode($query, true);
         $errorInfo = $json['errorInfo'];

         if(intval($errorInfo[0]) == 0)
         {
           $rowsKiosk['status_kiosk'] = $status;

           $rowsKiosk["action"]   = "U";
           $rowsKiosk = chkDataEmpty($rowsKiosk);

           $sql        = "SELECT * FROM t_agency WHERE agency_code = '$agency_code'";
           $query      = DbQuery($sql,null);
           $agency     = json_decode($query,true)['data'][0];

           $ipAgency = $agency['ip_center'];

           $data_array  = array(
                              "functionName" => "manageKiosk",  ///แก้ ชื่อ Service
                              "dataJson" => $rowsKiosk,
                            );
           $url        = "http://$ipAgency/ws/service.php";

           $make_call = callAPI('POST', $url, json_encode($data_array));
           $response = json_decode($make_call, true);
           $status   = $response['status'];
           $data     = $response['data'];

           $arrUpdate2['kiosk_id']     =  $kiosk_id; //แก้ ID
           $arrUpdate2['status_send']  =  "N";

           $sql = "";
           if($status == "200")
           {
               $arrUpdate2['status_send']  =  "S";
           }else{
               $arrLog['url']  = $url;
               $arrLog['data'] = json_encode($data_array,JSON_UNESCAPED_UNICODE);
               $arrLog['table_name'] = 't_trans_queue';
               $arrLog['id_update']  = 'trans_queue_id';
               $arrLog['date_create']  = date('Y/m/d H:i:s');
               $arrLog['data_update']  = json_encode($arrUpdate2);

               $sql = DBInsertPOST($arrLog,'t_log_send_service');
           }
           $sql .= DBUpdatePOST($arrUpdate,'t_kiosk','kiosk_id');
           //echo $sql;
           DbQuery($sql,null);

           $status   = 200;
           $message  = 'success';
           $data     = $rowsKiosk;
         }
      }else{
          $status   = 201;
          $message  = 'ไม่พบข่อมูล';
          $data     = array();
      }
    }else{
      $status   = 401;
      $message  = 'fail';
      $data     = array();
    }
?>

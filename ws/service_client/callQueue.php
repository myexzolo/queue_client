<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');

    // $status   = 200;
    // $message  = 'success';
    $dataJson     = isset($request['dataJson'])?$request['dataJson']:"";

    unset($request['type']);
    unset($request['functionName']);
    if($dataJson != "")
    {
      $point_id             = $dataJson['p'];
      $service_id_list      = $dataJson['sl'];
      $agency_code          = $dataJson['acode'];
      $trans_queue_id       = $dataJson['qid'];
      $service_channel      = $dataJson['ch'];


      $dateStart = date('Y-m-d')." 00:00:00";
      $dateEnd  = date('Y-m-d')." 23:59:59";

      $sql    = "SELECT q.trans_queue_id,q.queue_code,q.ref_code,q.date_start,q.type_service,q.menu_id,ser.service_name_a,
                        ser.kpi_time_a,mm.menu_name,ser.transfer_service_id_a,q.service_channel
                 FROM t_trans_queue q
                 LEFT JOIN t_service_agency ser ON q.service_id = ser.service_id
                 LEFT JOIN t_menu mm ON q.menu_id = mm.menu_id
                 where q.trans_queue_id = $trans_queue_id and q.status_queue = 'W'";

       $query     = DbQuery($sql,null);
       $json      = json_decode($query, true);
       $count     = $json['dataCount'];
       $errorInfo = $json['errorInfo'];
       $rowQueue  = $json['data'];

       if($count == 1)
       {
         $conDsv = "";
         $date_service = @$rowQueue[0]['date_service'];

         if($date_service == "")
         {
           $conDsv = ", date_service = NOW()";
         }

         $sql     = "UPDATE t_trans_queue set status_queue = 'R'  $conDsv , service_channel = '$service_channel' where trans_queue_id = '$trans_queue_id';";
         $sql    .= "UPDATE t_service_channel set status_active = 'V' where service_channel = '$service_channel' and point_id = '$point_id';";

         //echo $sql;
         $querys     = DbQuery($sql,null);
         $json       = json_decode($querys, true);
         $errorInfo  = $json['errorInfo'];
       }else{
         $sql    = "SELECT q.trans_queue_id,q.queue_code,q.ref_code,q.date_start,q.type_service,q.menu_id,ser.service_name_a,ser.kpi_time_a,mm.menu_name
                    FROM t_trans_queue q
                    LEFT JOIN t_service_agency ser ON q.service_id = ser.service_id
                    LEFT JOIN t_menu mm ON q.menu_id = mm.menu_id
                    WHERE q.status_queue = 'W' AND q.agency_code = '$agency_code'
                    AND q.date_start between '$dateStart' and '$dateEnd' and q.point_id = '$point_id'
                    AND
                     (case
                       when  q.type_service = 1 then q.menu_id in (select DISTINCT mn.menu_id
                                                                   from t_service_agency sv,t_menu mn
                                                                   where find_in_set(sv.service_id,mn.service_id_list) <> 0
                                                                   and sv.service_id in ($service_id_list))
                       when  q.type_service = 2 then q.service_id in ($service_id_list)
                       else 1
                     end)
                     AND
                      (case
                        when  q.service_channel is not null then q.service_channel = '$service_channel'
                        else q.service_channel is null
                      end)
                     $con
                     ORDER BY date_start LIMIT 0,1";

           $query  = DbQuery($sql,null);
           $json   = json_decode($querys, true);
           $count      = $json['dataCount'];
           $errorInfo  = $json['errorInfo'];
           $rowQueue   = $json['data'];

           if($count > 0)
           {
             $conDsv = "";
             $date_service = @$rowQueue[0]['date_service'];
             if($date_service == "")
             {
               $conDsv = ", date_service = NOW()";
             }

             $trans_queue_id  = $rowQueue[0]['trans_queue_id'];
             $sql        = "UPDATE t_trans_queue set status_queue = 'R' $conDsv , service_channel = '$service_channel'  where trans_queue_id = $trans_queue_id";
             $sql    .= "UPDATE t_service_channel set status_active = 'V' where service_channel = '$service_channel' and point_id = '$point_id';";
             $querys     = DbQuery($sql,null);
             $json       = json_decode($querys, true);
             $errorInfo  = $json['errorInfo'];
           }
       }


      if(intval($errorInfo[0]) == 0){
        if($count > 0){

          $sqls   = "SELECT * FROM t_trans_queue where trans_queue_id = '$trans_queue_id'"; //เปลี่ยน table
          $querys     = DbQuery($sqls,null);
          $json       = json_decode($querys, true);
          $rows       = $json['data'][0];

          $rows["action"]   = "U";
          $agency_code      = $rows["agency_code"];

          $rows = chkDataEmpty($rows);

          $sql        = "SELECT * FROM t_agency WHERE agency_code = '$agency_code'";
          $query      = DbQuery($sql,null);
          $agency     = json_decode($query,true)['data'][0];

          $ipAgency = $agency['ip_center'];

          $data_array  = array(
                             "functionName" => "manageTransQueue",  ///แก้ ชื่อ Service
                             "dataJson" => $rows,
                           );
          $url        = "http://$ipAgency/ws/service.php";

          //echo json_encode($data_array);

          $make_call = callAPI('POST', $url, json_encode($data_array));
          $response = json_decode($make_call, true);
          $status   = $response['status'];
          $data     = $response['data'];

          $arrUpdate['trans_queue_id']     =  $trans_queue_id; //แก้ ID
          $arrUpdate['ref_code']           =  $rows["ref_code"];
          $arrUpdate['status_send']        =  "N";

          $sql = "";
          if($status == "200")
          {
              $arrUpdate['status_send']  =  "S";
          }else{
              $arrLog['url']  = $url;
              $arrLog['data'] = json_encode($data_array,JSON_UNESCAPED_UNICODE);
              $arrLog['table_name'] = 't_trans_queue';
              $arrLog['id_update']  = 'trans_queue_id';
              $arrLog['date_create']  = date('Y/m/d H:i:s');
              $arrLog['data_update']  = json_encode($arrUpdate);

              $sql = DBInsertPOST($arrLog,'t_log_send_service');
          }
          $sql .= DBUpdatePOST($arrUpdate,'t_trans_queue','trans_queue_id');
          //echo $sql;
          DbQuery($sql,null);

          $status   = 200;
          $message  = 'success';
          $data     = $rowQueue;
        }else{
          $status   = 201;
          $message  = 'ไม่พบข้อมูล';
          $data     = null;
        }
      }else{
        $status   = 401;
        $message  = 'fail';
        $data     = null;
      }
      // $data = $dataJson;
    }

?>

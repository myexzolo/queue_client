<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');

    // $status   = 200;
    // $message  = 'success';
    $dataJson     = isset($request['dataJson'])?$request['dataJson']:"";

    unset($request['type']);
    unset($request['functionName']);
    if($dataJson != "")
    {
      $point_id             = $dataJson['p'];
      $service_id_list      = $dataJson['sl'];
      $agency_code          = $dataJson['acode'];
      $skipid               = $dataJson['skip'];
      $ch                   = $dataJson['ch'];

      $con = "";

      if($skipid != "")
      {
        $con = " and trans_queue_id not in ($skipid)";
      }

      $dateStart = date('Y-m-d')." 00:00:00";
      $dateEnd  = date('Y-m-d')." 23:59:59";

        $sqls   = "SELECT q.trans_queue_id,q.queue_code,q.ref_code,q.date_start,q.type_service,q.menu_id,
                          ser.service_name_a,ser.kpi_time_a,mm.menu_name,q.service_channel
                   FROM t_trans_queue q
                   LEFT JOIN t_service_agency ser ON q.service_id = ser.service_id
                   LEFT JOIN t_menu mm ON q.menu_id = mm.menu_id
                   WHERE q.status_queue = 'W' AND q.agency_code = '$agency_code'
                   AND q.date_start between '$dateStart' and '$dateEnd' and q.point_id = '$point_id'
                   AND
                    (case
                      when  q.type_service = 1 then q.menu_id in (select DISTINCT mn.menu_id
                                                                  from t_service_agency sv,t_menu mn
                                                                  where find_in_set(sv.service_id,mn.service_id_list) <> 0
                                                                  and sv.service_id in ($service_id_list))
                      when  q.type_service = 2 then q.service_id in ($service_id_list)
                      else 1
                    end)
                    AND
                     (case
                       when  q.service_channel is not null then q.service_channel = '$ch'
                       else q.service_channel is null
                     end)
                    $con
                    ORDER BY date_start";

      //echo $sqls;
      $querys = DbQuery($sqls,null);
      $json   = json_decode($querys, true);
      $counts     = $json['dataCount'];
      $errorInfo  = $json['errorInfo'];
      $rows       = $json['data'];


      if(intval($errorInfo[0]) == 0){
        if($counts > 0){
          $status   = 200;
          $message  = 'success';
          $data     = $rows;
        }else{
          $status   = 201;
          $message  = 'ไม่พบข้อมูล';
          $data     = null;
        }
      }else{
        $status   = 401;
        $message  = 'fail';
        $data     = null;
      }
      // $data = $dataJson;
    }

?>

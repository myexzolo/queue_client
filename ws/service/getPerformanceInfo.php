<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');

    $status = 200;
    $message = 'success';
    $dataJson  = isset($request['dataJson'])?$request['dataJson']:"";


    unset($request['type']);
    unset($request['functionName']);


    if($dataJson != "")
    {
        $ip         = $dataJson['ip'];
        $kiosk_code = $dataJson['kiosk_code'];

        $url     = "http://localhost:2021/ws/GetPerformanceInfo/".trim($kiosk_code);

        //echo $url;

        $data   = callAPI_Service('GET', $url, "");

        if($data)
        {
          $status   = 200;
          $message  = 'success';
        }else
        {
          $status   = 500;
        }
    }

?>

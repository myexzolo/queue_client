<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');

    // Turn on output buffering
    $dataJson     = isset($request['dataJson'])?$request['dataJson']:"";
    if($dataJson != "")
    {
      $ip         = "GETMAC /s ".$dataJson['ip'];
      ob_start();
      //Get the ipconfig details using system commond
      system($ip);

      // Capture the output into a variable
      $mycom=ob_get_contents();
      // Clean (erase) the output buffer
      ob_clean();

      $findme = "Device";
      // $findme = "Physical";
      //Search the "Physical" | Find the position of Physical text
      //echo   $mycom;
      $pmac = strpos($mycom, $findme);

      // Get Physical Address
      $mac=substr($mycom,($pmac-21),17);
      //Display Mac Address
      // echo "MAC4 : ".$mac;

      $status   = 200;
      $message  = 'success';
      $data['mac4'] = $mac;
    }

?>

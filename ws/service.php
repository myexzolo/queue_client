<?php
include('config.php');
$status = 404;
$message = 'functionName is not macth';
$data = array();

$postdata = file_get_contents("php://input");
$request = json_decode($postdata,true);
// $request = $_REQUEST;
// echo $request;
$functionName = @$request['functionName'];
switch (@$request['functionName']) {
  //ALL
  case 'manageMenu':
    include('service/manageMenu.php');
    break;
  case 'managePointService':
    include('service/managePointService.php');
    break;
  case 'manageServiceChanel':
    include('service/manageServiceChanel.php');
    break;
  case 'manageReserveQueue':
    include('service/manageReserveQueue.php');
    break;
  case 'manageServiceAgency':
    include('service/manageServiceAgency.php');
    break;
  case 'getPerformanceInfo':
    include('service/getPerformanceInfo.php');
    break;
  case 'manageTransQueue':
    include('service/manageTransQueue.php');
    break;
  case 'setUser':
    include('service/setUser.php');
    break;
  case 'manageKiosk':
    include('service/manageKiosk.php');
    break;
  case 'userLogin':
    include('service/userLogin.php');
    break;
  case 'settimeClose':
    include('service/settimeClose.php');
    break;
  case 'setStatusActiveChannel':
    include('service/setStatusActiveChannel.php');
    break;
  case 'manageReserve':
    include('service/manageReserve.php');
    break;
  case 'manageRole':
    include('service/manageRole.php');
    break;
  case 'serviceChannel':
    include('service/serviceChannel.php');
    break;
  case 'getMacAddress':
    include('service/getMacAddress.php');
    break;
  case 'manageKioskAgency':
    include('service_client/manageKioskAgency.php');
    break;
  case 'getQueue':
    include('service_client/getQueue.php');
    break;
  case 'callQueue':
    include('service_client/callQueue.php');
    break;
  case 'reCallQueue':
    include('service_client/reCallQueue.php');
    break;
  case 'endQueue':
    include('service_client/endQueue.php');
    break;
  case 'cancelQueue':
    include('service_client/cancelQueue.php');
    break;
  case 'sendQueue':
    include('service_client/sendQueue.php');
    break;
  case 'getServiceChannel':
    include('service_client/getServiceChannel.php');
    break;
  case 'transferQueue':
    include('service_client/transferQueue.php');
    break;
  case 'keepQueue':
    include('service_client/keepQueue.php');
    break;
  case 'getKeepQueue':
    include('service_client/getKeepQueue.php');
    break;
  case 'keepCallQueue':
    include('service_client/keepCallQueue.php');
    break;
  case 'setLastActive':
    include('service_client/setLastActive.php');
    break;


  default:
    break;
}
exit(json_encode(
  array( "status" => $status , "message" => $message ,
  "data" => $data ,"functionName" => $functionName  ) ));

?>
